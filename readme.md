# netsnake

This program shows how to run snake over UDP by running a client and server program.
The program is terminal based. 2 terminals are required, to run the server and the client side.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Build requirements

- CMake 3.1.6
- GNU C++ Compiler v9.4.0 or higher
- C++ Stdlib supporting C++17

## How to build

1. Clone the repo.
2. run `> cmake .`, initialized CMake cache
3. run `> cmake --build .`, builds the project
4. the root directory contains now the binary `netsnake`

### Debugging

One may switch to debug mode by commenting in line 10 in `CMakeLists.txt`. A rebuild of the program is necessary afterwards.

### Code Documentation

The code is documented in JavaDoc style. So Doxygen can generate a HTML documentation
from the comments.

1. From the root projetc folder navigate to `doc/`
2. Execute `doxygen netsnake.doc`
3. Navigate to `export/html`
4. Run `> firefox index.html`

## How to run

```bash
> ./netsnake --help
Usage: netsnake --run_as (server|client) [--help]
                [--ip_address IP_ADDRESS] [--port PORT] [--blocking]
                [--game_board_width WIDTH] [--game_board_height HEIGHT]
```

### CLI Options

- `--run_as` defines the role, `client` or `server`. It is the only mandatory argument
- `--help` displays help
- `--ip_address` defines the ip address to use as connection point, it defaults to `localhost` when not set
- `--port` defines the port number, must be `1024 < PORT < 2^16-1`, it defaults to `15000` when not set
- `--blocking` enables blocking socket behaviour, use not recommended, it defaults to `false` when not set
- `--game_board_width` defines the board width of snake game board. Must be between `12 <= WIDTH <= 30`, it defaults to `12` when not set
- `--game_board_height` defines the board width of snake game board. Must be between `12 <= HEIGHT <= 30`, it defaults to `12` when not set

The last 2 arguments are only of interest when run as server, other wise they'll be neglected.

#### Running the server

```bash
> ./netsnake --run_as server
```

This command will run the server side in default mode. It uses the localhost address to bind to on port `15000`. A successful start will run a 12x12 snake game board and looks like this:

```bash
############
# netsnake #
#   srv    #
#   0.1    #
############
#          #
#          #
#          #
#          #
#          #
#          #
#          #
#          #
#          #
#          #
############
```

The server is now waiting for a client to connect to `localhost:15000` and join the game.

#### Running the client

```bash
> ./netsnake --run_as client
```

This command will run the client side in default mode. It uses the localhost address to bind to on port `15000`. 
A successful start will display some informational output about the game controls and looks like this:

```bash
***********************
* netsnake client 0.1 *
***********************
*      q - quit       *
*      w - up         *
*      a - left       *
*      s - down       *
*      d - right      *
***********************
Your input:
```

The client waits now for the stated user input.

## The game

Start the server first, otherwise the client will end itself, if there is no response in a predefined time.
Then start the client. If the 'connection' succeeds, the client gets assigned a game ID by which the control
commands can be identified on the server side.

The client sends the keys `w, a, s, d` as up, left, down and right motion commands to the server.
It is not necessary to confirm motion commands by hitting return. They are sent right away.
`q` ends the client. Also when the players snake hits a wall or itself, the client will end itself.

Any 5 seconds a new food resource is generated on the map, by which the snake grows when it consumes it.

### Known bugs

- The program was only tested on a local machine since no second pc was available. So there is no guarentee this will work over ethernet network.
- When the client's snake dies, the socket thread is ended, but the main thread still asks for an input.
  Only after that the client will return.
- Only UDP packets with MTU < 1500 bytes are supported, hence the dimensional cap to 30 in width and height

### Compatibility

Only tested on Ubuntu Linux 20.04 LTS. No Windows or Mac support so far.
