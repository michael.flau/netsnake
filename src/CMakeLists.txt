target_sources(${NETSNAKE} PRIVATE
                netsnake_config.hpp
                common.hpp
                socket.hpp
                multi_socket.hpp
                message_io.hpp
                game.hpp
                server.hpp
                client.hpp
                main.cpp)
