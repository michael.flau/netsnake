/**
 * @author Michael Flau
 * @date december 2022
 * @brief assessment task for ZAL GmbH
 */

#ifndef CLIENT_HPP
#define CLIENT_HPP

// netsnake
#include "netsnake_config.hpp"
#include "socket.hpp"
// OS
#include <signal.h>
#include <termios.h>
// Std C++
#include <thread>
#include <sstream>

namespace netsnake {

/**
 * @brief The Client class
 * This class represents a game client. It's purpose is to bundle components logically
 * which represent in their entirety a complete client instance.
 */
class Client final {
    
    /*
     * Holds all custom client
     * credentials.
     */
    struct ClientCredentials {

        ClientCredentials(std::string ip_address, int16_t port, bool blocking):
        ip_address(ip_address),
        port(port),
        blocking(blocking){}

        std::string ip_address;
        int16_t port;
        bool blocking;
        struct termios initial; // initial key behaviour
        struct termios custom;  // app key behaviour
    };

    /*
     * Ctor
     */
    Client(const std::string &ip_address, const int16_t &port, bool blocking):
    mb_run(false),
    m_client_creds({ip_address, port, blocking}) {
    }

    public:

    /**
     * @brief
     * Destructor, resets terminal behaviour.
     */
    ~Client() {
#ifdef DEBUG
        std::cout << "Client is terminating...";
#endif
        reset_term();
#ifdef DEBUG
        std::cout << "...done!\n";
#endif

    }

    /**
     * @brief
     * Client factory method. Use this method
     * to retrieve a Client instance. Upon first call
     * the Client instance will be created. Succeeding calls
     * will yield the same instance.
     *
     * @return
     * Upon call returns a shared_ptr instance pointing to 
     * the created Client instance. If the creation failed,
     * the shared pointer will contain a nullptr.
     */
    [[nodiscard]]
    static std::shared_ptr<Client> create(const std::string &ip_address = "127.0.0.1",
                                          int16_t port = 15000,
                                          bool blocking = false) {
        using namespace std;

        if (!mp_instance) {
            auto pInstance = new Client(ip_address, port, blocking);

            if (!pInstance) {
                cerr << "Failed to allocate an instance of class Client!\n";
            }

            mp_instance.reset(pInstance);
        }

        if (!mp_instance) {
            std::cerr << "Invalid client instance!\n";
        }

        return mp_instance;
    }

    /**
     * @brief
     * Runs the client's main eventloop.
     * A call to this method, will start the client side,
     * cause it to attempt a connection to the server.
     * And request input from the user.
     */
    int run() {
       
        auto term_state = EXIT_SUCCESS;
#ifdef DEBUG
        std::cout << "Starting client...\n";
#endif

        // Initialize Server
        mb_run = init();
        
        if ( !mb_run ) {
            term_state = EXIT_FAILURE;
            return term_state;
        }
        
        //buffer size
        constexpr int buffer_sz(1000);

        // data structure which is shared between, server instance and socket
        auto socket_data = std::make_shared<SharedClientSocketData>();
        
        //create a server socket
        auto socket = ClientSocket(m_client_creds.ip_address,
                                   m_client_creds.port,
                                   netsnake::SockType::UDP,
                                   netsnake::ProtoType::NET,
                                   buffer_sz,
                                   m_client_creds.blocking ? 
                                   netsnake::SockBehaviour::BLOCKING : 
                                   netsnake::SockBehaviour::NON_BLOCKING,
                                   socket_data);
        

        // Socket mutex
        std::mutex socket_mutex;
        
        // Open socket
        socket.open();
        
        // Start socket thread, pass mutex to thread method
        std::thread socket_thread(&ClientSocket::transmit,
                                  std::addressof(socket),
                                  std::ref(socket_mutex));
        
        // Allowed user inputs
        const std::vector<std::string> allowed_inputs({"w", "a", "s", "d", "q", "c"});

        // main server loop
        while (mb_run) {

            /*
             * Check if socket thread indicates
             * that the client shall shutdown.
             */

            /////////////MUTEX LOCKED AREA///////////////////
            {
                std::lock_guard<std::mutex> lock(socket_mutex);
                if (socket_data->terminate) {
#ifdef DEBUG
                    std::cout << "Server requested client shutdown!\n";
#endif
                    mb_run = false;
                    continue;
                }
            }
            /////////////MUTEX LOCKED AREA///////////////////
        
            /*
             * Read user input, blocking
             */
            auto user_input_opt = read_user_input(allowed_inputs);

            if ( !user_input_opt ) {
                std::cerr << "Invalid user input, retry please.\n";
                continue;
            }

            auto user_input = user_input_opt.value();

            // End main loop if user hit 'q' or 'c'
            if (user_input == "q" || user_input == "c") {
                
                // flag main server loop to end
                mb_run = false;
                continue;
            } else {

                // Transform user input into a snake direction.
                SnakeDir dir;

                if (user_input == "w") {
                    dir = SnakeDir::UP;
                } else if (user_input == "a") {
                    dir = SnakeDir::LEFT;
                } else if (user_input == "s") {
                    dir = SnakeDir::DOWN;
                } else {
                    dir = SnakeDir::RIGHT;
                }

                /*
                 * Write new direction to socket thread.
                 */

                /////////////MUTEX LOCKED AREA///////////////////
                {
                    std::lock_guard<std::mutex> lock(socket_mutex);
                    socket_data->next_snake_dir = dir;
                    socket_data->dir_update = true;
                }
                /////////////MUTEX LOCKED AREA///////////////////
            }

            // Clear the terminal
            netsnake::clear_screen();
        }

        // let socket know, it's time to end
        /////////////MUTEX LOCKED AREA///////////////////
        {
            std::lock_guard<std::mutex> lock(socket_mutex);
            socket_data->terminate = true;
        }
        /////////////MUTEX LOCKED AREA///////////////////

        // Wait for thread to terminate here
        socket_thread.join();

        return term_state;
    }
    
    /**
     * @brief
     * Stops the server's main loop.
     * Called from signal_handler.
     */
    void stop() {
#ifdef DEBUG
        std::cout << "Stopping client instance...\n";
#endif
        mb_run = false;
    }

    protected:
   
    /**
     * @brief
     * Initializes the Client instance.
     * Installs a signal_handler to catch SIGINT
     * system events and modifies the terminal behaviour, so that
     * key strokes don't need a line break to be read.
     * @return boolean, true on success else false
     */
    bool init() {
#ifdef DEBUG
        std::cout << "Initializing server...\n";
#endif
        auto ret = signal(SIGINT, Client::signal_handler);

        if (ret == SIG_ERR) {
            std::cerr << strerror(errno);            
            return false;
        }

        // install custom terminal behaviour
        modify_term();

        return true;
    }
  
    /**
     * @brief
     * Change terminal behaviour, so that
     * key inputs do not need to be finalized
     * by hitting the return key.
     * @note calling this method, changes the behaviour
     * permanently. To revert the behaviour, Client::reset_term()
     * must be called.
     */
    void modify_term() {
#ifdef DEBUG
        std::cout << "Installing custom terminal behaviour...";
#endif
        tcgetattr(0, &m_client_creds.initial);

        m_client_creds.custom = m_client_creds.initial;
        m_client_creds.custom.c_lflag &= ~(ISIG|ICANON|ECHO);
        m_client_creds.custom.c_cc[VMIN] = 1;
        m_client_creds.custom.c_cc[VTIME] = 2;

        tcsetattr(0, TCSANOW, &m_client_creds.custom);
#ifdef DEBUG
        std::cout << "done!\n";
#endif
    }

    /**
     * @brief
     * Reset standard terminal behaviour, called in destructor.
     */
    void reset_term() {
#ifdef DEBUG
        std::cout << "Resetting terminal behaviour...";
#endif
        tcsetattr(0, TCSANOW, &m_client_creds.initial);
#ifdef DEBUG
        std::cout << "done!\n";
#endif
    }

    /*
     * Reads input from stdin. Only inputs defined in
     * input vector allowed_inputs will yield a processable
     * return value, otherwise the std::optional will be a nullopt
     * value.
     * @param allowed_inputs - const reference to std::vector of strings which
     * contains all allowed user input strings.
     * @return std::optional, containing the user input as std::string,
     * or a std::nullopt, when user input was not whitelisted by allowed_inputs. 
     */
    [[nodiscard]]
    std::optional<std::string> read_user_input
    (const std::vector<std::string> &allowed_inputs) {
      
        using namespace std;

        bool accepted = false;
        char user_input_c;
        string user_input_s;

        // print client prompt
        print_prompt();
        // read key input
        user_input_c = getchar();
        user_input_s.append(1, user_input_c);
        // probes whether input is int allowed inputs
        auto probe = [&user_input_s](const string &c) { 
            return c == user_input_s; 
        };

        // check if input is allowed
        accepted = any_of(allowed_inputs.cbegin(), allowed_inputs.cend(), probe); 

        if ( !accepted ) {
            cerr << "Invalid input!\n";
        }
        
        // return optional or nullopt based on result
        return accepted ? optional<string>(user_input_s) : nullopt;
    }

    /**
     * @brief
     * Prints a custom line promp to the screen
     * of the netsnake client. Contains some information
     * about valid keys.
     */
    void print_prompt() {
        
        std::string header("* netsnake client " + std::to_string(VERSION_MAJOR)
                           + "." + std::to_string(VERSION_MINOR) + " *");

        std::vector<std::string> bodies ({"q - quit",  \
                                          "w - up",  \
                                          "a - left",  \
                                          "s - down",  \
                                          "d - right"});

        std::stringstream ss;

        auto line = [&ss, &header] () {

            for (auto c: header) {
                ss << "*";
            }
            ss << "\n";
        };

        line();
        ss << header << "\n";
        line();
       
        int maxsz = 0;

        for (auto &l: bodies) {
            auto numw = header.size()-2;
            auto diff = numw - l.size();

            if (l.size() > maxsz) {
                maxsz = l.size();
            }
            auto lspace = maxsz / 2;
            auto rspace = maxsz / 2;

            while ((lspace + l.size() + rspace) < numw) {
                ++rspace;
            }

            ss << "*" << std::setw(lspace+2) << " " << l << std::setw(rspace-2) << " " << "*\n";

        }

        line();
        std::cout << ss.rdbuf();
        std::cout << "Your input: \n";
    }

    /*
     * Signal handler which listens for SIGINT interrupt
     */
    static void signal_handler(int sigint) {
#ifdef DEBUG
        std::cout << "Received signal!\n";
#endif
        if (sigint != SIGINT) return;
        mp_instance->stop();
    }

    /*
     * Member
     */
    bool mb_run;
    ClientCredentials m_client_creds;

    /*
     * Static server instance shared across all instances
     */
    inline static std::shared_ptr<Client> mp_instance;
};

}

#endif //CLIENT_HPP
