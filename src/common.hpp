/**
 * @author Michael Flau
 * @date december 2022
 * @file common.cpp
 * Contains all logic not belonging to any class.
 * This file stores mostly helper logic, static functions
 * and data structures, which need to be available over the whole
 * codebase.
 */

#ifndef COMMON_HPP
#define COMMON_HPP
#include <iostream>
#include <optional>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <cstdlib>
#include <chrono>
#include <cassert>
#include <typeinfo>
#include <atomic>
#include <sstream>
#include <cstdint>

#ifdef _WIN32
#include <conio.h>
#endif

namespace netsnake {

    /****************
     * MACRO HELPER *
     ****************/
#define STRINGIFY(v) std::string(#v)

    /*******************
     * TEMPLATE HELPER *
     *******************/

    /*
     * Convenience template, to cast
     * enum class values to their underlying 
     * integer type.
     */
    template<typename T>
    constexpr typename std::underlying_type<T>::type etul(T value) {
        return static_cast<typename std::underlying_type<T>::type>(value);
    }

    /*********
     * ENUMS *
     *********/

    /**
     * @brief
     * Defines different types of
     * in-game objects.
     */
    enum class GameObjectType: uint8_t {
        WALL   = 0, ///> defines a wall
        SBODY  = 1, ///> defines a snake's body
        SHEAD  = 2, ///> defines a snake's head
        FOOD   = 3, ///> defines a food resource
        VACANT = 4, ///> defines a vacant slot on game board
        LBREAK = 5  ///> defines a linebreak character
    };

    /**
     * @brief
     * Defines possible movement directions
     * for a snake.
     */
    enum class SnakeDir: uint8_t {
        UP    = 0, ///> UPward direction
        DOWN  = 1, ///> DOWNward direction
        LEFT  = 2, ///> LEFTside direction
        RIGHT = 3  ///> RIGHTside direction
    };

    /**
     * @brief
     * These enum values represent
     * the type of message a server can
     * receive from a client.
     */
    enum class ClientMessageType:uint8_t {
        REQ_JOIN  = 0, ///> Client asks to join game
        REQ_CLOSE = 1, ///> Client asks to close connection
        UPDATE    = 2, ///> Client sends game update
        CONFIRM   = 3, ///> Client sends confirmation, e.g. on CON_CLOSE
    };

    /**
     * @brief
     * These enum values represent 
     * the type of message a client can receive
     * from a server.
     */
    enum class ServerMessageType:uint8_t {
        WELCOME   = 0, ///> Welcome message
        HOLD      = 1, ///> Wait for empty game slot
        UPDATE    = 2, ///> Game Update
        CON_CLOSE = 3, ///> Server request client disconnects
        CONFIRM   = 4  ///> Confirms a client request e.g. REQ_CLOSE
    };

    /********************
     * STREAM OVERLOADS *
     ********************/

    std::ostream& operator<<(std::ostream& os, uint8_t value) {
        os << static_cast<int>(value);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, ServerMessageType value) {
        os << static_cast<int>(value);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, ClientMessageType value) {
        os << static_cast<int>(value);
        return os;
    }

    std::ostream& operator<<(std::ostream& os, SnakeDir value) {
        os << static_cast<int>(value);
        return os;
    }

    /*******************
     * DATA STRUCTURES *
     *******************/

    /**
     * @brief
     * This data structure represents a message
     * as it is send by the client to the server
     */
    struct ClientMessage {
        int      id;                ///> ID of client's game session
        ClientMessageType mtype;    ///> The type of the message
        bool     has_new_dir;       ///> Indicates a new direction
        bool     plans_food_update; ///> Indicates that a food update is planned
        bool     has_food_update;   ///> Indicates that a food update was sent
        int      food_pos;          ///> The position of the new food ressource
        SnakeDir new_dir;           ///> The new direction of the client't snake
    };

    /**
     * @brief
     * This data structure represents a message
     * as it is send by the server, to a client.
     */
    struct ServerMessage {
        int  id;                    ///> ID of the client's game session
        ServerMessageType mtype;    ///> The type of the message
        bool player_dead;           ///> Indicates that the player's snake is dead
        bool has_map_update;        ///> Indicates that the message contains a map state update
        std::vector<int> map_state; ///> The container holding the map state update
    };

    /**
     * @brief
     * This data structure is shared between server socket instances
     * and their owning parent instances. The structure is used to
     * exchange data between threads.
     */
    struct SharedSocketData {
        SharedSocketData():
            terminate(false),
            client_update(false),
            server_update(false),
            client_messages(),
            server_messages(){}
        std::atomic_bool terminate;                 ///> Indicates whether the server shall shutdown
        std::atomic_bool client_update;             ///> Indicates whether the client has sent an update
        std::atomic_bool server_update;             ///> Indicates whether the game has sent an update
        std::vector<ClientMessage> client_messages; ///> Container holding all available client messages
        std::vector<ServerMessage> server_messages; ///> Container holding all available game messages
    };
    
    /**
     * @brief
     * This data structure is shared between client socket instances
     * and their owning parent instances. The structure is used to
     * exchange data between threads.
     */
    struct SharedClientSocketData {
        SharedClientSocketData():
            terminate(false),
            dir_update(false),
            next_snake_dir(SnakeDir::UP){}

        std::atomic_bool terminate;     ///> Indicates whether the client shall shutdown
        std::atomic_bool dir_update;    ///> Indicates a direction update from the client's main loop
        SnakeDir next_snake_dir;        ///> Holds the new directional update
    };

    /***************************
     * STATIC HELPER FUNCTIONS *
     ***************************/

    /*
     * Some helper functions for trimming strings
     * See: https://stackoverflow.com/questions/216823/how-to-trim-an-stdstring
     */
    
    // trim from start (in place)
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                std::not1(std::ptr_fun<int, int>(std::isspace))));
    }

    // trim from end (in place)
    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(),
                std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string &s) {
        rtrim(s);
        ltrim(s);
    }
    
    /**
     * @brief
     * Clear the terminal from any characters.
     * Implements platform specific ways to
     * clear a terminal window.
     */
    static inline void clear_screen() {
#ifdef _WIN32
        clrscr();
#elif (__LINUX__) || (__linux__) || (__gnu_linux__)
        // See https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
        std::cout << u8"\033[2J\033[1;1H";
#endif
    }

    /**
     * @brief generates a random number between a lower and an upper bound
     * @param lower - Lower boundary of range, within the random integer will be generated
     * @param upper - Upper boundary or range, within the random integer will be generated
     * @return std::optional containing a signed integer between lower and upper boundary. If
     * input boundaries are faulty, returns std::nullopt.
     */
    [[nodiscard]] static inline auto 
    randn(int lower, int upper) -> std::optional<int> {
        
        if ( lower >= upper ) {
            assert(lower < upper);
            std::cerr << "Lower boundary must be smaller than upper boundary!\n";
            return std::nullopt;
        }

        // get a unix timestamp
        const auto t          = std::chrono::system_clock::now();
        const unsigned unix_t = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();

        // set seed value
        srand(unix_t);

        // generate random value
        return std::optional<int>(lower + (rand() % ((upper - lower) + 1)));
    }
}

#endif //COMMON_HPP
