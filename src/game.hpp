/**
 * @author Michael Flau
 * @date december 2022
 * @brief assessment task for ZAL GmbH
 */

#ifndef GAME_HPP
#define GAME_HPP

//netsnake
#include "netsnake_config.hpp"
#include "common.hpp"
//STL
#include <cmath>

namespace netsnake {

    /*
     * Defines a single in-game object 
     */
    class GameObject {

        public:

        /*
         * Ctor
         */
        GameObject(const GameObjectType &type, const int &pos):
        m_type(type),
        m_position(pos){

        }

        /**
         * @brief simple getter to extract position
         * @return returns position as positive integer value
         */
        int pos() noexcept {
            return m_position;
        }
        
        int pos() const noexcept {
            return m_position;
        }

        /**
         * @brief simple setter to alter position of a game object
         * @param pos const reference to a new position value as integer
         */
        void set_pos(const int &pos) {
            m_position = pos;
        }
       
        /**
         * @brief simple getter, to acknowledge the GameObject instance's type.
         * @return GameObjectType value
         */
        GameObjectType type() {
            return m_type;
        }
        
        void set_type(GameObjectType t) {
            m_type = t;
        }

        protected:

        /*
         * Member
         */
        GameObjectType m_type;      ///> Game object type
        int            m_position;  ///> Game object position
    }; 

    /**
     * @brief The GameEntity class
     * Abstract base class, which holds common logic
     * required by all game entities.
     */
    class GameEntity {
        
        public:

            GameEntity(const int &width, const int &height):
                m_board_width(width),
                m_board_height(height),
                m_body() {
                
                    assert(m_board_width > 0);
                    assert(m_board_height > 0);
            }

            GameEntity(const GameEntity &other) {
                this->m_board_width = other.m_board_width;
                this->m_board_height = other.m_board_height;
                this->m_body.clear();
                this->m_body.reserve(other.m_body.size());
                this->m_body.insert(begin(this->m_body), begin(other.m_body), end(other.m_body));
            }

            GameEntity(GameEntity &&other) noexcept {
                this->m_board_width = other.m_board_width;
                other.m_board_width = 0;
                this->m_board_height = other.m_board_height;
                other.m_board_height = 0;

                this->m_body = std::move(other.m_body);
            }

            GameEntity& operator=(const GameEntity &rhs) {
                
                if ( this == &rhs ) {
                    return *this;
                }

                this->m_board_width = rhs.m_board_width;
                this->m_board_height = rhs.m_board_height;
                this->m_body.clear();
                this->m_body.reserve(rhs.m_body.size());
                this->m_body.insert(begin(this->m_body), begin(rhs.m_body), end(rhs.m_body));
                return *this;
            }

            GameEntity& operator=(GameEntity &&rhs) noexcept {
                
                if ( this == &rhs ) {
                    return *this;
                }
                
                this->m_board_width = rhs.m_board_width;
                rhs.m_board_width = 0;
                this->m_board_height = rhs.m_board_height;
                rhs.m_board_height = 0;

                this->m_body = std::move(rhs.m_body);

                return *this;
            }

            /**
             * @brief
             * Give access to internal GameObject vector
             * of a mutable GameEntity instance.
             * @return reference std::vector<GameObject>
             */
            virtual std::vector<GameObject>& get_game_objects() {
                return m_body;
            }
            
            /**
             * @brief
             * Access the internal GameObject vector of a immutable
             * GameEntity instance.
             * @return reference to const std::vector<GameObject>
             */
            virtual const std::vector<GameObject>& get_game_objects() const {
                return m_body;
            }

        protected:

            /**
             * @brief returns the maximum position in
             * the game board.
             */
            int max_pos() {
                return m_board_width * m_board_height;
            }

            /**
             * @brief
             * transforms a 1D positional value into a 2D positional value
             */
            inline std::optional<std::pair<int, int>> to_xy(const int &pos) {

                if (pos < 0 || pos > this->max_pos()) {
                    std::cerr << "Passed position value is invalid!\n";
                    assert(pos > 0);
                    assert(pos < this->max_pos());
                    return std::nullopt;
                }

                auto const x = pos % m_board_width;
                auto const y = (pos - x) / m_board_width;
                return std::optional<std::pair<int,int>>(std::make_pair(x,y));
            }

            /**
             * @brief
             * Internal sanitizer, to check position for correctness.
             */
            bool sanitize_position(const int &pos) {
                
                auto p_opt = to_xy(pos);

                if (!p_opt) {
                    std::cerr << "Invalid positional value!\n";
                    return false;
                }
                
                auto p2d = p_opt.value();

                if (p2d.first <= 0 || p2d.first >= m_board_width) {
                    std::cerr << "Positition is not within board dimensions: Board(" << m_board_width 
                              << "," << m_board_height << ") vs. Point(" << p2d.first 
                              << "," << p2d.second << ")\n";
                    return false;
                }
                
                if (p2d.second <= 0 || p2d.second >= m_board_height) {
                    std::cerr << "Positition is not within board dimensions: Board(" << m_board_width 
                              << "," << m_board_height << ") vs. Point(" << p2d.first 
                              << "," << p2d.second << ")\n";
                    return false;
                }

                return true;
            }

            /**
             * @brief 
             * Pure virtual base class method, must 
             * be impleneted by any leaf class. Supposed
             * to initialize the instance.
             */
            virtual bool init() = 0;


            /*
             * Members
             */
            int m_board_width;
            int m_board_height;
            std::vector<GameObject> m_body;
    };

    /*
     * @brief The Snake class
     * This class contains all logic and data to
     * build a playable Snake entity.
     */
    class SnakeEntity: public GameEntity {
        
        public:

        /*
         * Ctor
         */
        SnakeEntity(int start_pos, int board_width, int board_height, SnakeDir dir):
            GameEntity(board_width, board_height),
            m_id(SnakeEntity::next_id()),
            m_initial_head_pos(start_pos),
            m_direction(dir)
            {
           
            // initialize snake instance
            if ( !init() ) {
                std::cerr << "Failed to initialize snake instance!\n";
            }
        }

        /**
         * @brief Move the snake instance by one field.
         */
        virtual void move() {

            assert(m_body.size() > 1);

            /*
             * Move head
             */

            // keep back old position for later
            int old_pos = m_body.front().pos();
            int new_pos = old_pos; 
            
            switch(m_direction) {
                case SnakeDir::UP:
                    new_pos -= m_board_width;
                    break;
                case SnakeDir::DOWN:
                    new_pos += m_board_width;
                    break;
                case SnakeDir::LEFT:
                    new_pos -= 1;
                    break;
                case SnakeDir::RIGHT:
                    new_pos += 1;
                    break;
            };

            // now set new head position
            m_body.front().set_pos(new_pos);

            /*
             * Move body by removing last element
             * and adding a new body element a former
             * head position.
             */

            // Emplace new body element at former head position
            // assumes that whole snake is always > 1
            m_body.emplace(begin(m_body)+1, GameObject(GameObjectType::SBODY, old_pos));
            // remove last element
            m_body.pop_back();
        }

        /**
         * @brief Set new direction of snake instance.
         */
        virtual void set_direction(const SnakeDir &dir) {
           
            // holds whitelisted directions
            std::vector<SnakeDir> whitelist;

            /*
             * Directions which are allowed based
             * on current direction are stored in
             * whitelist.
             */
            switch(m_direction) {
                case SnakeDir::UP:
                case SnakeDir::DOWN:
                    whitelist = {SnakeDir::LEFT, SnakeDir::RIGHT};
                    break;
                case SnakeDir::LEFT:
                case SnakeDir::RIGHT:
                    whitelist = {SnakeDir::DOWN, SnakeDir::UP};
                    break;
            };

            // lambda probe, used in std::any_of
            auto probe = [&dir](const SnakeDir &allowed) {
                return dir == allowed;
            };

            /*
             * if any value in whitelist matches new direction
             * the new directional value is stored. Any other case
             * ignores the new direction, as it is either the same
             * or the contrary direction to the current, which is
             * prohibited.
             */
            if ( std::any_of(whitelist.begin(), whitelist.end(), probe) ) {
                m_direction = dir;
            }
        }
    
        /**
         * @brief
         * Simple getter, to retrieve the SnakeEntity instance's
         * ID.
         * @return instance ID as positive singed integer
         */
        virtual int id() noexcept {
            return m_id;
        }
        
        virtual int id() const noexcept {
            return m_id;
        }
        /**
         * @brief 
         * Convenience method, access the snakes head postion.
         * @returns integer - the snake instance's head position 
         * as signed integer
         */
        virtual int head_pos() {
            return m_body.front().pos();
        }

        /**
         * @brief
         * Checks if snake instance collided with any other game entity.
         * @param game_entity - The instance of GameEntity, to check against
         */
        virtual bool collided_with_entity(const GameEntity &game_entity) {
            return this->collided_with(game_entity.get_game_objects());
        }

        /**
         * @brief Access snake instances body parts
         * @return returns a mutable reference to Snake instances body parts
         */
        virtual std::vector<GameObject>& get_game_objects() override {
            return m_body;
        }

        /**
         * @brief add a body segment to the end of the snake.
         */
        virtual void add_segment() {
            this->grow();
        }

        protected:
       
        /**
         * Generates unique ids for snake entities
         */
        static inline int next_id() noexcept {
            static int id_counter = 0;
            return ++id_counter;
        }

        /**
         * @brief 
         * Check if the snake instance collided with any game object
         * in the passed vector. This collision check is based on the fact,
         * that a snake instance can only collide with 
         */
        bool collided_with(const std::vector<GameObject> &entity_gos) {

            auto probe = [this](const GameObject &entity_part){
                            return this->head_pos() == entity_part.pos();
                         };

            return std::any_of(cbegin(entity_gos), cend(entity_gos), probe);
        }

        /**
         * @brief Adds new body segment to the end of the snake
         * param pos Position of the new segment, defaults to -1.
         * If defualt value passed, the last current body part segment is used
         * as position reference to attach a new segment, otherwise the passed
         * position value is used.
         */
        void grow(int pos = -1) {

            int bp_pos{pos};

            if (pos == -1) {

                // initial pos is pos of last element
                bp_pos = m_body.back().pos();

                /*
                 * @todo, this might not be enough,
                 * as we cannot be sure if the new segment
                 * might be created on a position where some
                 * other game object exists. Additional
                 * checks should be in place, to use an alternative
                 * position in case, the initial pos is blocked.
                 */
                switch(m_direction) {
                    case SnakeDir::UP:
                        bp_pos += m_board_width;
                        break;
                    case SnakeDir::DOWN:
                        bp_pos -= m_board_width;
                        break;
                    case SnakeDir::LEFT:
                        bp_pos += 1;
                        break;
                    case SnakeDir::RIGHT:
                        bp_pos -= 1;
                        break;
                };
            }

            GameObject bp(GameObjectType::SBODY, bp_pos);
            m_body.push_back(bp);
        }

        /*
         * Initializes the snake instance.
         * The snake has an initial length of 3 at the beginning.
         * The initial direction is defined by the caller.
         */
        bool init() override {
            
            // head
            m_body.push_back(GameObject(GameObjectType::SHEAD, m_initial_head_pos));

            // body
            for (auto &offset: {1, 2}) {

                int body_pos = m_initial_head_pos;

                switch(m_direction) {
                    case SnakeDir::UP:
                        body_pos += offset * m_board_width;
                        break;
                    case SnakeDir::DOWN:
                        body_pos -= offset * m_board_width;
                        break;
                    case SnakeDir::LEFT:
                        body_pos += offset;
                        break;
                    case SnakeDir::RIGHT:
                        body_pos -= offset;
                        break;
                };
                
                this->grow(body_pos);
            }

            return true;
        }

        /*
         * Members
         */
        int m_id;
        int m_initial_head_pos;
        SnakeDir m_direction;
    };

    /**
     * @brief the FoodStashEntity class
     * This class represents a container for all food
     * objects in the game. It is constructed with a user defined
     * amount of food. Food may be removed or added at any point in time later.
     */
    class FoodStashEntity: public GameEntity {
        
        public:
        
        /*
         * Ctor
         */
        FoodStashEntity(const int &width, const int &height, const int &initial_food = 0):
        GameEntity(width, height),
        m_initial_food(initial_food) {

            if ( !init() ) {
                std::cerr << "Failed to create the food stash!\n";
            }
        }

        /**
         * @brief
         * Add a game object of type FOOD to FoodStashEntity instance.
         * @note
         * This method cannot fill more than 80% of the board area 
         * with food objects. If the stash is full further calls to
         * this method are irgnored until there is space in the stash
         * again.
         * @param pos - Position of the GameObject to add.
         * @return boolean - true on successful addition to food stash, false if not.
         */
        bool add_food(const int &pos) {

            bool state = sanitize_position(pos);

            if (!state) {
                std::cerr << "Invalid food position\n";
                assert(state);
                return state;
            }

            auto food_present = static_cast<float>(m_body.size());
            auto max_food     = static_cast<int>(0.8 * (static_cast<float>(m_board_width-2) * 
                                static_cast<float>(m_board_height-2)));

            if (food_present >= max_food) {
                return state;
            }

            GameObject food(GameObjectType::FOOD, pos);
            m_body.push_back(food);
            return state;
        }

        /**
         * @brief
         * Remove a game object of type FOOD from the FoodStashEntity instance.
         * @note
         * If the food stash is empty, no food object will be removed. The operation
         * will still be successfull.
         * @param Position of the GameObject stored, to be removed.
         * @return boolean - true on successful removal, false if removal failed.
         */
        bool remove_food(const int &pos) {
            
            bool state = sanitize_position(pos);

            if (!state) {
                std::cerr << "Invalid food position\n";
                assert(state);
                return state;
            }

            // Lambda to find food object with correct position
            auto probe = [&pos](GameObject &food){
                return food.pos() == pos && food.type() == GameObjectType::FOOD;
            };

            // Find food object in food stash
            auto it = std::find_if(m_body.begin(), m_body.end(), probe);

            // Erase if found
            if (it != m_body.end()) {
                m_body.erase(it);
            } 
#ifdef DEBUG
            else {
                std::cout << "No food at pos " << pos << "\n";
            }
#endif
            
            return state;
        }

        protected:

        /**
         * @brief
         * Initializes the FoodStashEntity instance.
         * Generates as many GameObjects of type FOOD,
         * as were specified during construction.
         * @return boolean - always true for now, might turn false later
         */
        virtual bool init() override {
            
            //create as many food objects as stated at construction time
            for (int idx = 0; idx < m_initial_food; ++idx) {
              
                const auto default_low = 1;
                // generate as x and y here, to 
                // stick within game board
                auto x_opt = randn(1, m_board_width -  1);
                auto y_opt = randn(1, m_board_height - 1);

                auto x( x_opt.value_or(default_low) );
                auto y( y_opt.value_or(default_low) );
#ifdef DEBUG
                std::cout << "Food at " << x << "," << y << "\n";
#endif
                auto food = GameObject(GameObjectType::FOOD, y * m_board_width + x);

                m_body.push_back(food);
            }

            return true;
        }

        int m_initial_food;
    };

    /**
     * @brief The GameBorderEntity class
     * This class produces and manages the surrounding 
     * game border for the game board.
     */
    class GameBorderEntity: public GameEntity {
        
        public:
        
        GameBorderEntity(const int &width, const int &height):
            GameEntity(width, height) {
            
            if ( !init() ) {
                std::cerr << "Failed to initialize the GameBorderEntity instance!\n";
            }
        }

        protected:
        
        /**
         * @brief
         * Initializes the GameBorderEntity instance.
         * Generates as many GameObjects of type WALL,
         * as needed to cover the game board area specified at construction time.
         * @return boolean - always true for now, might turn false later
         */
        virtual bool init() override {

            /*
             * Create border objects
             */
            for ( auto idx = 0; idx < m_board_height; ++idx ) {
                
                // First and last line need objects for each tile
                if (idx == 0 || idx == m_board_height - 1) {
                    
                    for (auto idx2 = 0; idx2 < m_board_width; ++idx2) {
                        m_body.push_back(GameObject(GameObjectType::WALL, idx * m_board_width + idx2));
                    }

                    continue;
                }
                
                // all lines in between need only 2 objects at start and end
                m_body.push_back(GameObject(GameObjectType::WALL, idx * m_board_width));
                m_body.push_back(GameObject(GameObjectType::WALL, (((idx + 1) * m_board_width) - 1)));
            }

            return true;
        }
    };

    /**
     * @brief The SnakeGame class
     * This class represents the game snake. The output is terminal based.
     * The game relies on input messages from external ressources, to control
     * aspects of the game.
     */
    class SnakeGame {

        public:

        SnakeGame(const int &width, const int &height, const int &max_snakes = 1):
        m_board_width(width),
        m_board_height(height),
        m_max_snakes(max_snakes),
        mb_map_state_requested(false),
        m_game_border(width, height),
        m_food_stash(width, height),
        m_snakes(),
        m_input_messages(),
        m_output_messages() {
            if ( !init() ) {
                std::cerr << "Failed to initialize SnakeGame instance!";
            }
        }

        /**
         * @brief
         * Setter method, to pass client updates to the game instance.
         * @param client_messages - rvalue reference to a vector of ClientMessage instances.
         */
        void set_client_input(std::vector<ClientMessage> client_messages) {
            m_input_messages = client_messages;
#ifdef DEBUG
            for (auto &m: m_input_messages) {
                std::cout << "New message for snake with id " << m.id << "\n";
            }
#endif
        }

        /**
         * @brief
         * Getter method, to extract any game messages, which need
         * to be forward to connected clients.
         * @return vector of ServerMessage instances 
         */
        [[nodiscard]]
        std::optional<std::vector<ServerMessage>> get_client_output() {

            if (m_output_messages.size() == 0) {
                return std::nullopt;
            }
#ifdef DEBUG
            std::cout << "Returning " << m_output_messages.size() << " messages for clients.\n";
#endif
            return std::optional<std::vector<ServerMessage>>(m_output_messages);
        }

        /**
         * @brief
         * Main processing method of the SnakeGame class.
         * Call this method to advance the Game by one step.
         * Internally it will call all required methods,
         * to carry out a complete game step, which are
         *
         * 1. Computing the next game step.
         * 2. Rendering the game state to the terminal
         * 3. Process any final message preparations to clients.
         */
        void run_step() {

            // first do next game step
            process_game_step();

            // render current game state
            render();

            // prepare a message update for clients
            prepare_client_update();

        }

        protected:
        
        /**
         * @brief
         * Initialization method. This method is a stub for now, but here anyway
         * for possible future initaliztion steps.
         */
        [[nodiscard]]
        bool init() {
            return true;
        }
       
        /**
         * @brief
         * Convenience wrapper method, to bundle all processing steps into one call.
         * First processes any client input messages. The moves all snake instances
         * by one field in their set directions. Finally checks for any collisions
         * which might occured by moving the snakes forward.
         * Called in server's main run method.
         */
        void process_game_step() {
           
            // Handle all client messages and 
            // apply processing steps to game entities.
            process_client_messages();

            // Move snakes
            for ( auto &snake: m_snakes) {
                snake.move();
            }

            // Handle collision actions
            process_collisions();
        }

        /**
         * @brief
         * Renders an informational header over the game board.
         * @param sstream - reference to std::stringstream input stream of frame being rendered.
         * @param frame - const reference to character which displays a frame piece
         * @param lbreak - const reference to character which displays a line break
         */
        void render_game_board_header(std::stringstream &sstream, 
                                      const char &frame = '#',
                                      const char &lbreak = '\n') {

            auto rl_offsets = [](const float word_sz, const float board_w){
                auto d = std::abs(board_w - word_sz);
                auto r = static_cast<int>(d) % 2;
                auto o = 0;
                if ( r > 0 ) ++o;
                return std::make_pair(std::floor((d-r) / 2.0), o);
            };


            for (auto idx = 0; idx < this->m_board_width; ++idx) {
                sstream << frame;
            }

            sstream << lbreak;
            int diffh = 0;            
            std::string game_text = "netsnake srv " + std::to_string(VERSION_MAJOR) + "." + std::to_string(VERSION_MINOR);

            if (game_text.size() < (this->m_board_width - 3)) {
                sstream << frame;
                auto p = rl_offsets(game_text.size(), this->m_board_width - 2);
                sstream << std::setw(p.first) << " " << game_text << std::setw(p.first + p.second) << " ";
                sstream << frame;
                sstream << lbreak;
            } else {
                std::stringstream wstream(game_text);
                std::string word;
                while(wstream >> word) {
                    sstream << frame;
                    diffh = (std::abs(static_cast<int>(this->m_board_width - word.size())) - 2) / 2;
                    int offset = 0;
                    while (((2*diffh) + word.size() + offset) < (this->m_board_width-2)) ++offset;
                    sstream << std::setw(diffh) << " " << word << std::setw((diffh) + offset) << " ";
                    sstream << frame;
                    sstream << lbreak;
                }
            }
        }

        /**
         * @brief
         * Prints the game board to stdout.
         * Must be called after every call of
         * SnakeGame::process_game_step to reflect
         * all changes on the game board, which
         * happended in this step.
         */
        void render() {
      
            // clear screen
            netsnake::clear_screen();

            int pos{-1};
            bool slot_rendered{false};

            /*
             * Fetch all game objects
             */

            // copy helper
            auto copy_gos = [](std::vector<GameObject> &target, std::vector<GameObject> &source){
                target.reserve(source.size());
                target.insert(begin(target), begin(source), end(source));
            };

            auto game_objects = std::vector<GameObject>();

            // copy wall
            copy_gos(game_objects, m_game_border.get_game_objects());
            
            // copy food
            copy_gos(game_objects, m_food_stash.get_game_objects());
            
            // copy snakes
            for ( auto snake: m_snakes) {
                copy_gos(game_objects, snake.get_game_objects());
            }

            std::sort(begin(game_objects), end(game_objects), [] (GameObject &a, GameObject &b) {
                return a.pos() > b.pos();
            });

            /*
             * Build string frame
             */
            std::stringstream frame;
            const std::array<char, 6> game_symbols({'#','s','h','*',' ','\n'});

            // build header
            render_game_board_header(frame,
                                     game_symbols[etul(GameObjectType::WALL)],
                                     game_symbols[etul(GameObjectType::LBREAK)]);

            // build game board
            for (auto idx_y = 0; idx_y < m_board_height; ++idx_y) {
                for ( auto idx_x = 0; idx_x < m_board_width; ++idx_x ) {
            
                    // compute 1D pos
                    pos = idx_y * m_board_width + idx_x;

                    /*
                     * For current position iterate over all
                     * game objects (go). If go position != current
                     * position, empty, else print go's type matching
                     * symbol.
                     */
                    for (auto &go: game_objects) {
                      
                        // empty slot
                        if (go.pos() != pos) {
                            continue;
                        }

                        // some game slot
                        frame << game_symbols[etul(go.type())];
                        slot_rendered = true;
                        break;
                    }

                    // render whitespace if no game object present
                    if ( !slot_rendered ) {
                        frame << game_symbols[etul(GameObjectType::VACANT)];
                    }
                    
                    slot_rendered = false;
                }

                // line break after every line
                frame << game_symbols[etul(GameObjectType::LBREAK)];
            }

            // "Blit" frame to screen
            std::cout << frame.rdbuf();
        }
       
        /**
         * @brief
         * Processes all collision actions of active SnakeEntity instances regardless
         * of the game object whith which the snake instance collides.
         */
        void process_collisions() {

            // All snakes which collided with a wall or another snake must be
            // removed from the list of active snakes. This vector will store
            // all IDs of all snakes which need to be terminated.
            std::vector<int> kill_list;

            /* 
             * Check collisions for 
             * each active snake.
             */
            for (auto it_snake = begin(m_snakes); it_snake != end(m_snakes); ++it_snake) {

                // next snake entity
                auto &snake = (*it_snake);
                
                /*
                 * Check for wall collisions
                 */
                if (snake.collided_with_entity(m_game_border)) {
#ifdef DEBUG
                    std::cout << "Snake collided with wall!\n";
#endif
                    kill_list.push_back(snake.id());
                    continue;
                }

                /*
                 * Check for snake collisions
                 */

                // determines if loop cycle ends prematurely
                bool skip = false;

                for (auto &other_snake: m_snakes) {

                    /* 
                     * Corner case: snake collides with itself.
                     * Here all but the head position must be
                     * compared.
                     */
                    if (snake.id() == other_snake.id()) {
                        auto go_ref = other_snake.get_game_objects();
                        for (auto go_it = begin(go_ref)+1; go_it != end(go_ref); ++go_it) {
                            if (snake.head_pos() == go_it->pos()) {
#ifdef DEBUG
                                std::cout << "Snake collided with itself!\n";
#endif
                                kill_list.push_back(snake.id());
                                skip = true;
                                break;
                            }
                        }
                       
                        if (skip) {
                            // if true, we have collision, so we don't 
                            // need to check for food collisions
                            break;
                        } else {
                            // skip rest of comparison or head position 
                            // always collides with itself on same snake IDs
                            continue;
                        }
                    }

                    if ( snake.collided_with_entity(other_snake) ) {
#ifdef DEBUG
                        std::cout << "Snake with ID " << it_snake->id() << " collided with snake with ID " << other_snake.id() << "\n";
#endif
                        kill_list.push_back(snake.id());
                        break;
                        skip = true;
                    }
                }

                if ( skip ) continue;

                /*
                 * Check collision with food
                 */
                
                if ( snake.collided_with_entity(m_food_stash) ) {
                    // Snake grows by one
                    snake.add_segment();

                    //remove food at snake's head position
                    m_food_stash.remove_food(snake.head_pos());
                }
            }

            /*
             * Remove all snakes in kill list 
             * from active snake vector.
             */
            for (auto &snake_id: kill_list) {
#ifdef DEBUG
                std::cout << "Removing snake with ID " << snake_id << "\n";
#endif
                if ( !remove_snake(snake_id) ) {
                    std::cerr << "Failed to remove snake with id " << snake_id << "\n";
                }
            }
        }

        /**
         * @brief
         * This function processes all messages
         * received from clients and applies the
         * commands contained in the messages to
         * their associated game entities.
         */
        void process_client_messages() {

            // delete all old server messages in buffer
            // just to be on the safe side.
            m_output_messages.clear();

            for (auto msg: m_input_messages) {
              
                auto it = begin(m_snakes);

                if ( msg.id != -1 ) {
                    it = find_active_snake(msg.id);

                    if ( it == end(m_snakes) ) {
                        std::cerr << "No snake with that ID, ignoring...\n";
                        continue;
                    }
                } else {
                    // if the id is -1, the only allowed action is that
                    // the client asks to join the game
                    assert(msg.mtype == ClientMessageType::REQ_JOIN);
#ifdef DEBUG
                    std::cout << "New player requests to join the game!\n";
#endif
                }

                switch(msg.mtype) {
                    
                    /*
                     * The client asks to join the game.
                     * Here, the client either be permitted,
                     * if the maximum number of participants
                     * is not reached yet, or the client
                     * is denied access. In the first case,
                     * a WELCOME message is sent, other wise
                     * the CON_CLOSE message will be sent.
                     *
                     * @todo
                     * later the client should be put on hold,
                     * and notified, when another client left, 
                     * so that the waiting client can join.
                     */
                    case ClientMessageType::REQ_JOIN:
                        {
                            if (m_snakes.size() < m_max_snakes) {
#ifdef DEBUG
                                std::cout << "Preparing WELCOME message to invite player to the game!\n";
#endif
                                // add a snake instance to game
                                auto msg_type = ServerMessageType::WELCOME;
                                auto snake_id_opt = create_snake();

                                if ( !snake_id_opt ) {
                                    msg_type = ServerMessageType::CON_CLOSE;
                                    std::cerr << "Failed to create new snake, rejecting client instead.\n";
                                } 
#ifdef DEBUG
                                else {
                                    std::cout << "Snake has ID " << snake_id_opt.value() << "\n";
                                }
#endif
                                // prepare game server response
                                auto response = ServerMessage({
                                            snake_id_opt.value_or(msg.id),
                                            msg_type,
                                            false,
                                            false,
                                            {}
                                        });

                                // store response in response vector
                                m_output_messages.push_back(response);
                            } else {
#ifdef DBEUG
                                std::cout << "Rejecting client with CON_CLOSE, game is full!\n";
#endif
                                // reject client, full
                                auto response = ServerMessage({
                                            msg.id,
                                            ServerMessageType::CON_CLOSE,
                                            false,
                                            false,
                                            {}
                                        });

                                // store response in response vector
                                m_output_messages.push_back(response);
                            }
                        }
                    break;
                    /*
                     * The client is about to close the connection,
                     * so the active snake instance is removed, and
                     * the message is confirmed.
                     */
                    case ClientMessageType::REQ_CLOSE:
                    {
                        auto msg_type = ServerMessageType::CONFIRM;

                        // when removal fails, the snake instance does not exist,
                        // so we tell the client to exit under any circumstances.
                        if (!remove_snake(msg.id)) {
#ifdef DEBUG
                           std::cout << "Failed to remove snake, try to get rid of client.\n"; 
#endif               
                           msg_type = ServerMessageType::CON_CLOSE; 
                        }

                        ServerMessage response({msg.id,
                                                msg_type,
                                                false,
                                                false,
                                                {}});

                        // Store message in response vector
                        m_output_messages.push_back(response);
                    }
                    break;
                    /*
                     * The client has send an update with control
                     * information of the snake instance. Further
                     * the client might request a food update.
                     */
                    case ClientMessageType::UPDATE:
                    {
#ifdef DEBUF
                        std::cout << "Update for snake with id " << msg.id << "\n";
#endif
                        auto snake_it = find_active_snake(msg.id);

                        if ( snake_it == end(m_snakes) ) {
                            std::cerr << "Client send ID which does not exist!\n";
                            continue;
                        }

                        /*
                         * Clients can alter the snakes
                         * direction, they indicate this with
                         * a flag.
                         */
                        if ( msg.has_new_dir ) {
#ifdef DEBUF
                            std::cout << "New direction for snake!\n";
#endif
                            snake_it->set_direction(msg.new_dir);
                        }

                        // both must never be true
                        assert(!(msg.plans_food_update && msg.has_food_update));

                        /*
                         * Clients can either plan a food update soon,
                         * or have a food update ready.
                         */
                        if ( msg.plans_food_update ) {
#ifdef DEBUF
                            std::cout << "Client requested game map state!\n";
#endif
                            mb_map_state_requested = true;
                        } else if ( msg.has_food_update ) {
                            m_food_stash.add_food(msg.food_pos);
                        }

                        // Prepare a confirm response 
                        auto msg_type = ServerMessageType::CONFIRM;
                        ServerMessage response({msg.id,
                                                msg_type,
                                                false,
                                                false,
                                                {}});

                        // Store message in response vector
                        m_output_messages.push_back(response);

                    }
                    break;
                    case ClientMessageType::CONFIRM:
#ifdef DEBUF
                        std::cout << "Client with ID " << msg.id << " confirmed!\n";
#endif
                    break;
                }
            }

            // Delete all messages in input buffer
            m_input_messages.clear();
        }

        /**
         * @brief
         * Helper method to find an active snake in all active snakes.
         * @param id - const reference to a signed integer, which represents an ID of a snake instance.
         * @return std::vector<SnakeEntity>::iterator which points to the found snake instance or
         * to the end of the snake instance vector, if no snake was found.
         */
        std::vector<SnakeEntity>::iterator find_active_snake(const int &id) {
                
            auto find_active_snake = [&id](const SnakeEntity &se){
                    return id == se.id();
            };

            return std::find_if(begin(m_snakes), end(m_snakes), find_active_snake);
        }

        /**
         * @brief
         * Remove a snake from active snake buffer.
         * @param id - ID value of the snake to be deleted.
         * @return boolean - true if snake was removed, false if anything went wrong.
         */
        bool remove_snake(const int &id) {
            
            auto it = find_active_snake(id);
            bool ret{it != end(m_snakes)};

            if (ret) {

                // Modify message to let player know that snake is dead
                bool modified = false;
                for (auto &m: m_output_messages) {
                    if (m.id == id) {
#ifdef DEBUG
                        std::cout << "Marking snake with ID " << id << " as dead!\n";
#endif
                        m.mtype = ServerMessageType::CON_CLOSE;
                        m.player_dead = true;
                        modified = true;
                    }
                }

                // create new message if no update message exists
                if ( !modified ) {
#ifdef DEBUG
                    std::cout << "Marking snake with ID " << id << " as dead! (B)\n";
#endif
                    ServerMessage msg({id, ServerMessageType::CON_CLOSE, true, false, {}});
                    m_output_messages.push_back(msg);
                }

                // Erase snake entity from game
                m_snakes.erase(it);
            }

            return ret;
        }

        /**
         * @brief
         * Creates a new snake and adds it to vector of active snake instances.
         * @note Max number of snakes must not be active or a call to this method
         * does nothing.
         * @return std::optional<int> - if creation succeeded, an optional containing
         * the snake instance id is returned, otherwise returns nullopt.
         */
        [[nodiscard]]
        std::optional<int> create_snake() {
            
            if (m_snakes.size() == m_max_snakes ) {
                return std::nullopt;
            }

            auto sx  = randn(3, m_board_width  -3).value_or(m_board_width  * 0.5);
            auto sy  = randn(3, m_board_height -3).value_or(m_board_height * 0.5);
            auto dir = static_cast<SnakeDir>(randn(0, 3).value_or(0));

            SnakeEntity s(sy*m_board_width + sx,
                          m_board_width,
                          m_board_height,
                          dir);

            m_snakes.push_back(s);
            return std::optional<int>(s.id());
        }

        /**
         * @brief
         * Prepares all client updates which can only
         * be done after a game step. Must always be called
         * as last method in game step method.
         */
        void prepare_client_update() {
            
            /*
             * Preparing map state vector
             * for clients.
             */
            if (mb_map_state_requested) {

                // fill with 1 initially, all unblocked
                std::vector<int> map_state(m_board_width*m_board_height, 1);

                assert(m_board_height*m_board_width == map_state.size());

                // mark wall
                auto go_ref = m_game_border.get_game_objects();

                for (auto it = begin(go_ref); it != end(go_ref); ++it) {
                    ++map_state[it->pos()];
                }

                // mark food
                go_ref = m_food_stash.get_game_objects();

                for (auto it = begin(go_ref); it != end(go_ref); ++it) {
                    ++map_state[it->pos()];
                }

                for (auto &snake: m_snakes) {
                    // mark snakes
                    go_ref = snake.get_game_objects();

                    for (auto it = begin(go_ref); it != end(go_ref); ++it) {
                        ++map_state[it->pos()];
                    }
                }

                /*
                 * write map state to output messages
                 */
                for (auto msg_it = begin(m_output_messages); msg_it != end(m_output_messages); ++msg_it) {
                    msg_it->map_state = map_state;
                    msg_it->has_map_update = true;
                }
            }

            mb_map_state_requested = false;


        }


        int                        m_board_width;
        int                        m_board_height;
        int                        m_max_snakes;
        bool                       mb_map_state_requested;
        GameBorderEntity           m_game_border;
        FoodStashEntity            m_food_stash;
        std::vector<SnakeEntity>   m_snakes;
        std::vector<ClientMessage> m_input_messages;
        std::vector<ServerMessage> m_output_messages;
    };
}

#endif //GAME_HPP
