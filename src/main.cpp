/**
 * @author Michael Flau
 * @date december 2022
 * @file main.cpp 
 * Main entry point for application netsnake
 */

// net_snake 
#include "server.hpp"
#include "client.hpp"
// cmake
#include "netsnake_config.hpp"
// Std C++
#include <iostream>

using cli_opt_t = std::tuple<netsnake::AppRole, std::string, int16_t, bool, int, int>;

/**
 * @brief
 * Prints help dialog to stdout and exits application.
 */
void help() {
    constexpr int sw(16);
    std::cout << "netsnake version " << VERSION_MAJOR << "." << VERSION_MINOR << "\n";
    std::cout << "Usage: netsnake --run_as (server|client) [--help]\n";
    std::cout << std::setw(sw) << " " << "[--ip_address IP_ADDRESS] [--port PORT] [--blocking]\n";
    std::cout << std::setw(sw) << " " << "[--game_board_width WIDTH] [--game_board_height HEIGHT]\n";
    exit(EXIT_SUCCESS);
}

/**
 * @brief
 * Simple argument parser for CLI args.
 * @param argc - Argument count
 * @param argv - CLI arguments
 * @return Hash with all arguments, containing an 
 *         ARG value as id and a std::optional as value.
 */
auto parse_args(int argc, char **argv) -> std::optional<cli_opt_t> {

    using namespace netsnake;

    if (argc < 3) {
        assert(argc < 3);
        help();
    }

    std::vector<std::string> cli_args;

    // read args into vector
    for (int idx = 0; idx < argc; ++idx) {
        cli_args.push_back( std::string(argv[idx]) );
    }

    constexpr int min_dim(12);
    constexpr int max_dim(30);
    constexpr int min_port(1024);

    bool got_role(false);
    auto role(AppRole::SERVER);
    std::string ip("127.0.0.1");
    int16_t port(15000);
    bool blocking(false);
    int mwidth(12);
    int mheight(12);

    // Check for args
    for ( auto it = cli_args.begin(); it != cli_args.end(); ++it ) {
        if ( *it == "--help" ) {
            help();
        }

        if ( *it == "--run_as" ) {

            if ( *std::next(it) == "client" ) {
                got_role = true;
                role = netsnake::AppRole::CLIENT;
            } else if ( *std::next(it) == "server") {
                got_role = true;
                role = netsnake::AppRole::SERVER;
            }
        }

        if ( *it == "--ip" ) {
            if ( std::next(it)->size() == 15 ) {
                ip = *it;                
            } 
        }

        if ( *it == "--port" ) {
            auto v = stoi(*std::next(it));
            if ( v > min_port && v < std::numeric_limits<int16_t>::max() ) {
                port = v;
            }
        }

        if ( *it == "--blocking" ) {
            blocking = true;
        }

        if ( *it == "--game_board_width" ) {
            auto v = stoi(*std::next(it));

            if (v >= min_dim && v <= max_dim ) {
                mwidth = v;
            }
        }

        if ( *it == "--game_board_height" ) {
            auto v = stoi(*std::next(it));

            if (v >= min_dim && v <= max_dim ) {
                mheight = v;
            }
        }

    }

    return got_role ? std::make_optional<cli_opt_t>(
                std::make_tuple(role, ip, port, blocking, mwidth, mheight)
                ) : std::nullopt;
}

/*
 * App main entry point
 */
int main(int argc, char **argv) {

    auto role_opt = parse_args(argc, argv);

    if ( !role_opt ) {
        std::cerr << "Invalid command line arguments. Terminating.\n";
        return EXIT_FAILURE;
    }
    
    auto options = role_opt.value();

    switch(std::get<0>(options)) {
        case netsnake::AppRole::SERVER:
            {
                auto server = netsnake::Server::create(std::get<1>(options),
                                                       std::get<2>(options),
                                                       std::get<3>(options),
                                                       std::get<4>(options),
                                                       std::get<5>(options));

                if (!server) {
                    std::cerr << "Failed to create server. Terminating.\n";
                    return EXIT_FAILURE;
                }

                return server->run();
            }
        break;
        case netsnake::AppRole::CLIENT:
            {
                auto client = netsnake::Client::create(std::get<1>(options),
                                                       std::get<2>(options),
                                                       std::get<3>(options));

                if ( !client ) {
                    std::cerr << "Failed to create client. Terminating.\n";
                    return EXIT_FAILURE;
                }

                return client->run();
            }
        break;
    };
}

