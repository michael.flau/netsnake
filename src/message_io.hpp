/**
 * @author Michael Flau
 * @date december 2022
 * @file message_io.hpp
 * Contains all logic required to
 * serialize and deserialize text and object based messages from
 * client and server to their associated pendants.
 */

#ifndef MESSAGE_IO_HPP
#define MESSAGE_IO_HPP

//netsnake
#include "common.hpp"

namespace netsnake {

class MessageProcessor {

    public:
    
        MessageProcessor(const char field_delimiter = ';', const char &key_value_delimiter = '='):
            m_field_delimiter(field_delimiter),
            m_key_value_delimiter(key_value_delimiter) {
        
        }

    protected:

        std::vector<std::string> split_into_fields(const std::string &message) {
           
            std::istringstream is(message);
            std::string tmp;
            std::vector<std::string> output;

            while(std::getline(is, tmp, m_field_delimiter)) {
                output.push_back(tmp);
            }

            return output;
        }

        std::pair<std::string, std::string> split_into_kv_pair(const std::string &field, const char &delim) { std::istringstream is(field); std::string tmp; std::vector<std::string> output;

            while(std::getline(is, tmp, delim)) {
                output.push_back(tmp);
            }

            assert(output.size() == 2);

            return std::make_pair(output[0], output[1]);
        }

        char m_field_delimiter;
        char m_key_value_delimiter;
};

class ServerMessageProcessor: public MessageProcessor {
        
    public:
        ServerMessageProcessor(const char field_delimiter = ';', const char &key_value_delimiter = '='):
            MessageProcessor(field_delimiter, key_value_delimiter) {

            }

        std::string serialize(const ServerMessage &server_message) {
            
            std::stringstream ss;
            
            // stringify id
            auto tmp = STRINGIFY(server_message.id);
            ss << split_into_kv_pair(tmp , '.').second;
            ss << m_key_value_delimiter;
            ss << server_message.id;
            ss << m_field_delimiter;

            // stringify message type
            tmp = STRINGIFY(server_message.mtype);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << server_message.mtype;
            ss << m_field_delimiter;

            // player dead bool 
            tmp = STRINGIFY(server_message.player_dead);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << static_cast<int>(server_message.player_dead);
            ss << m_field_delimiter;

            // has map update bool
            tmp = STRINGIFY(server_message.has_map_update);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << static_cast<int>(server_message.has_map_update);
            ss << m_field_delimiter;

            // map update
            tmp = STRINGIFY(server_message.map_state);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            if ( server_message.has_map_update) {
                for ( auto &n: server_message.map_state) {
                    ss << n;
                }
            } else { //represents empty set
                ss << "-1";
            }
            
            return ss.str();
        }

        /**
         * @brief
         * Deserializes a server message into a ServerMessage object.
         * @param stringified_server_message - const reference to std::string,
         *        containing the stringified server message.
         * @return Instance of ServerMessage with parsed message content.
         */
        ServerMessage deserialize(const std::string &stringified_server_message) {

            auto fields = split_into_fields(stringified_server_message);
            ServerMessage ret;
            
            for (auto &field: fields) {
                
                auto kv_str = split_into_kv_pair(field, m_key_value_delimiter);

                if (kv_str.first == "id") {
                    ret.id = std::stoi(kv_str.second);
                } else if (kv_str.first == "mtype") {
                    ret.mtype = static_cast<ServerMessageType>(std::stoi(kv_str.second));
                } else if (kv_str.first == "player_dead") {
                    ret.player_dead = std::stoi(kv_str.second);
                } else if (kv_str.first == "has_map_update") {
                    ret.has_map_update = std::stoi(kv_str.second);
                } else if (kv_str.first == "map_state") {

                    if (kv_str.second == "-1") continue;

                    for (char &nc: kv_str.second) {
                        ret.map_state.push_back(nc-'0');
                    }
                }
            }

            return ret;
        }
};

/**
 *
 */
class ClientMessageProcessor: public MessageProcessor {

    public:

        /**
         *
         */
        ClientMessageProcessor(const char field_delimiter = ';', const char &key_value_delimiter = '='):
            MessageProcessor(field_delimiter, key_value_delimiter) {

            }

        /**
         *
         */
        std::string serialize(const ClientMessage &client_message) {
            
            std::stringstream ss;
            
            // stringify id
            auto tmp = STRINGIFY(client_message.id);
            ss << split_into_kv_pair(tmp , '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.id;
            ss << m_field_delimiter;

            // stringify message type
            tmp = STRINGIFY(client_message.mtype);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.mtype;
            ss << m_field_delimiter;

            // stringify has_new_dir 
            tmp = STRINGIFY(client_message.has_new_dir);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.has_new_dir;
            ss << m_field_delimiter;

            // stringify plans_food_update 
            tmp = STRINGIFY(client_message.plans_food_update);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.plans_food_update;
            ss << m_field_delimiter;
            
            // stringify has_food_update 
            tmp = STRINGIFY(client_message.has_food_update);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.has_food_update;
            ss << m_field_delimiter;
            
            // stringify food_pos 
            tmp = STRINGIFY(client_message.food_pos);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.food_pos;
            ss << m_field_delimiter;

            // stringify new_dir 
            tmp = STRINGIFY(client_message.new_dir);
            ss << split_into_kv_pair(tmp, '.').second;
            ss << m_key_value_delimiter;
            ss << client_message.new_dir;

            return ss.str();
        }

        /**
         *
         */
        ClientMessage deserialize(const std::string &stringified_client_message) {
            
            auto fields = split_into_fields(stringified_client_message);
            ClientMessage ret;
            
            for (auto &field: fields) {
                
                auto kv_str = split_into_kv_pair(field, m_key_value_delimiter);

                if (kv_str.first == "id") {
                    ret.id = std::stoi(kv_str.second);
                } else if (kv_str.first == "mtype") {
                    ret.mtype = static_cast<ClientMessageType>(std::stoi(kv_str.second));
                } else if (kv_str.first == "has_new_dir") {
                    ret.has_new_dir = std::stoi(kv_str.second);
                } else if (kv_str.first == "plans_food_update") {
                    ret.plans_food_update = std::stoi(kv_str.second);
                } else if (kv_str.first == "has_food_update") {
                    ret.has_food_update = std::stoi(kv_str.second);
                } else if (kv_str.first == "food_pos") {
                    ret.food_pos = std::stoi(kv_str.second);
                } else if (kv_str.first == "new_dir") {
                    ret.new_dir = static_cast<SnakeDir>(std::stoi(kv_str.second));
                }
            }

            return ret;
        }
};
}
#endif // MESSAGE_IO_HPP
