/**
 * @author Michael Flau
 * @date december 2022
 * @brief Contains all logic which is used by multiplexing sockets.
 */

#ifndef SOCKET_HPP
#define SOCKET_HPP

#include "socket.hpp"

namespace netsnake {

    class MultiplexingSocket: public Socket {

        MultiplexingSocket (const std::string_view ip_address, 
               const int16_t &port, 
               const SockType &stype, 
               const ProtoType &ptype, 
               const int &buffer_sz,
               const SockBehaviour &sockb = SockBehaviour::MULTIPLEXING_POLL)):
        Socket(ip_address, 
               port, 
               stype, 
               ptype, 
               buffer_sz,
               sockb)) {

        }
    };
    
}
#endif //SOCKET_HPP


