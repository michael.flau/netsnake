/**
 * @author Michael Flau
 * @date december 2022
 * @brief assessment task for ZAL GmbH
 */

#ifndef SERVER_HPP
#define SERVER_HPP

// netsnake
#include "socket.hpp"
#include "game.hpp"
#include "message_io.hpp"
// OS
#include <signal.h>
#include <errno.h>
// Std C++
#include <mutex>
#include <thread>

namespace netsnake {

/**
 * @brief The Server class
 * This class represents a game server. It's purpose is to bundle components logically
 * which represent in their entirety a complete server instance.
 * 
 * @todo add multi client support.
 *
 * @note 
 * This class is a singleton. You can not create more than one instance of this class.
 */
class Server final {

    /*
     * Internal convenience struct
     * to store user defined data.
     */
    struct ServerCredentials {
        std::string ip_address;
        int16_t port;
        bool blocking_behaviour;
        int board_width;
        int board_height;
        int game_step_delay;
    };

    /*
     * Ctor
     */
    Server(const std::string &ip_address, 
           const int16_t &port, 
           bool blocking_behaviour = false, 
           const int &game_board_width = 12, 
           const int &game_board_height = 12,
           const int &gs_delay = 500):
    mb_run(false),
    m_srv_creds({ip_address, 
                 port, 
                 blocking_behaviour, 
                 game_board_width, 
                 game_board_height,
                 gs_delay}){
        
        ///@todo Port and IP Adress check to be added!
    }
 
    public:
    
    /**
     * @brief
     * Server factory method. Use this method
     * to retrieve a Server instance. Upon first call
     * the server instance will be created. Succeeding calls
     * will yield the same instance.
     *
     * @return
     * Upon call returns a shared_ptr instance pointing to 
     * the created server instance. If the creation failed,
     * the shared pointer will contain a nullptr.
     *
     * @note
     * The return value must not be ignored or the compiler
     * is going to emit an error.
     */
    [[nodiscard]] static std::shared_ptr<Server> 
    create(const std::string &ip_address, 
           const int16_t &port, 
           bool blocking_behaviour = false, 
           const int &game_board_width = 12, 
           const int &game_board_height = 12,
           const int &gs_delay = 500) {
      
        using namespace std;

        if (!mp_instance) {
            auto pInstance = new Server(ip_address, 
                                        port, 
                                        blocking_behaviour, 
                                        game_board_width, 
                                        game_board_height, 
                                        gs_delay);

            if (!pInstance) {
                cerr << "Failed to allocate an instance of class Server!\n";
            }

            mp_instance.reset(pInstance);
        }

        if (!mp_instance) {
            std::cerr << "Invalid server instance!\n";
        }

        return mp_instance;
    }

    /**
     * @brief
     * Main server processing loop
     */
    int run() {

        auto term_state = EXIT_SUCCESS;

#ifdef DEBUG
        std::cout << "Starting server...\n";
#endif
        // Initialize Server
        mb_run = init();
        
        if ( !mb_run ) {
            term_state = EXIT_FAILURE;
            return term_state;
        }
        
        // socket buffer size
        constexpr int buffer_sz = 1000;
        // runtime in seconds
        int runtime_sec = 0;
        // server loop delay in ms
        auto game_step_delay = std::chrono::duration<int, std::milli>(m_srv_creds.game_step_delay);
   
        /*
         * Create socket and run in dedicated thread
         */

        // data structure which is shared between, server instance and socket
        auto socket_data = std::make_shared<SharedSocketData>();
        
        // Socket mutex
        std::mutex socket_mutex;
        
        //create a server socket
        auto socket = ServerSocket(m_srv_creds.ip_address,
                                   m_srv_creds.port,       
                                   netsnake::SockType::UDP,
                                   netsnake::ProtoType::NET,
                                   buffer_sz, 
                                   m_srv_creds.blocking_behaviour ?
                                   netsnake::SockBehaviour::BLOCKING : 
                                   netsnake::SockBehaviour::NON_BLOCKING,
                                   socket_data);

        // Open socket
        socket.open();
        
        // Start socket thread, pass mutex to thread method
        std::thread socket_thread(&ServerSocket::await_connections,
                                  std::addressof(socket),
                                  std::ref(socket_mutex));

        // Start a game instance
        SnakeGame snake(m_srv_creds.board_width, m_srv_creds.board_height);
        
        // main server loop
        while (mb_run) {
            /////////////MUTEX LOCKED AREA///////////////////
            {
                std::lock_guard<std::mutex> lock(socket_mutex);
                if (socket_data->terminate) {
                    std::cout << "Client requested server shutdown!\n";
                    this->stop();
                    continue;
                }
            }
            
            // 1. Lock shared data structure
            // 2. Read new message from structure
            //////////////MUTEX LOCKED AREA//////////////////
            if (socket_data->client_update) {
                std::lock_guard<std::mutex> lock(socket_mutex);
#ifdef DEBUG
                std::cout << "Passing client messages to game!\n";
#endif
                snake.set_client_input(socket_data->client_messages);
                socket_data->client_messages.clear();
                socket_data->client_update = false;
            }
            // 3. Unlock data structure
            /////////////MUTEX LOCKED AREA///////////////////
            // 4. Process next game step
            snake.run_step();
            // 5. Prepare message for connected clients
            // 6. Send message to client(s)
            /////////////MUTEX LOCKED AREA///////////////////
            auto ret_opt = snake.get_client_output();
            if (ret_opt) {
                std::lock_guard<std::mutex> lock(socket_mutex);
                socket_data->server_messages = ret_opt.value();    
                socket_data->server_update = true;
            }
            /////////////MUTEX LOCKED AREA///////////////////
            // put thread to sleep
            std::this_thread::sleep_for(game_step_delay);
        }

        // Let socket know it shall terminate
        {
#ifdef DEBUG
            std::cout << "Terminating server socket...\n";
#endif
            std::lock_guard<std::mutex> lock(socket_mutex);
            socket_data->terminate = true;
        }

        // wait for socket thread to finish
        socket_thread.join();
        return term_state;
    }

    /**
     * @brief
     * Stops the server's main loop.
     */
    void stop() {
#ifdef DEBUG
        std::cout << "Stopping server instance...\n";
#endif
        mb_run = false;
    }

    protected:

    /*
     * Initializes all server components.
     */
    bool init() {
#ifdef DEBUG
        std::cout << "Initializing server...\n";
#endif
        auto ret = signal(SIGINT, Server::signal_handler);

        if (ret == SIG_ERR) {
            std::cerr << strerror(errno);            
            return false;
        }

        return true;
    }
   
    /*
     * Signal handler which listens for SIGINT interrupt
     */
    static void signal_handler(int sigint) {
#ifdef DEBUG
        std::cout << "Received signal!\n";
#endif 
        if (sigint != SIGINT) return;
        mp_instance->stop();
    }

    /*
     * Member
     */
    bool mb_run;
    ServerCredentials m_srv_creds;
    /*
     * Static server instance shared across all instances
     */
    inline static std::shared_ptr<Server> mp_instance;
};

}

#endif //SERVER_HPP
