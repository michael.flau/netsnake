/**
 * @author Michael Flau
 * @date december 2022
 * @brief assessment task for ZAL GmbH
 */

#ifndef SOCKET_HPP
#define SOCKET_HPP

// netsnake
#include "common.hpp"
#include "message_io.hpp"
#include "timer.hpp"
// OS
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <poll.h>
// Std C++
#include <memory>
#include <string>
#include <string_view>
#include <limits>
#include <vector>
#include <iomanip>
#include <mutex>
#include <iterator>

namespace netsnake {

    ///> Defines the role the running app instance is in
    enum class AppRole: uint8_t {
        SERVER = 0, ///> Server role
        CLIENT = 1  ///> Client role
    };

    ///> Defines the socket connection type
    enum class SockType: uint8_t {
        TCP = 0, ///> TCP protocol
        UDP = 1  ///> UDP protocol
        // extend if necessary    
    };

    ///> Defines the socket type
    enum class ProtoType: uint8_t {
        NET = 0,  ///> maps to AF_INET
        LOCAL = 1 ///> maps to AF_UNIX
        // extend if necessary
    };

    ///> Defines the socket behaviour
    enum class SockBehaviour: uint8_t {
        BLOCKING,           ///> Simple blocking connection
        NON_BLOCKING,       ///> Simple non blocking connection
        MULTIPLEXING_POLL,  ///> Mulitplexing connection using poll()
        MULITPLEXING_SELECT ///> Multiplexing connection using select()
    };

    /**
     * @brief The Socket class
     * this class represents an abstractional layer between
     * the OS network functionality and any client code, requiring
     * access to to a socket interface.
     *
     * The class supports local and network sockets of protocol
     * types UDP and TCP.
     */
    class Socket {
       
        /*
         * Internal data structure
         * to keep all socket related 
         * state values in one place.
         */
        struct SocketCredentials {
            
            SocketCredentials(int buffer_sz):
                is_open(false),
                buffer_size(buffer_sz),
                socket_descriptor(-1),
                proto_family(-1),
                socket_type(-1),
                port(-1),
                ip_address(),
                buffer(buffer_sz, 0) {

            }

            bool is_open;
            int buffer_size;
            int socket_descriptor;
            int proto_family;
            int socket_type;
            int16_t port;
            std::string_view ip_address;
            std::vector<char> buffer;
            struct sockaddr_in saddrin;
            struct sockaddr_in udp_client_address;
            SockBehaviour socket_behaviour;
        };

        public:
        
        /*
         * ctor
         */
        Socket(const std::string_view ip_address, 
               const int16_t &port, 
               const SockType &stype, 
               const ProtoType &ptype, 
               const int &buffer_sz,
               const SockBehaviour &sockb = SockBehaviour::BLOCKING):
            m_socket_creds(buffer_sz),
            m_smp(),
            m_cmp() {
           
            /*
             * Initialize socket descriptor
             */

            auto sock_opt = create_socket(stype, ptype, sockb);

            if (!sock_opt) {
                std::cerr << "Failed to create socket!\n";
                return;
            }

            m_socket_creds.socket_descriptor = sock_opt.value();


            /*
             * Port
             */

            // 1024 denotes well known port boundary
            auto status = port > 1024 && port < std::numeric_limits<int16_t>::max();
            
            if ( !status ) {
                std::cerr << "Invalid port number: " << port << "\n";
                assert(status);
                return;
            }

            m_socket_creds.port = port;

            /*
             * IP Address
             */

            /// @todo Add proper IP string check here.
            m_socket_creds.ip_address = ip_address;


            /*
             * Store protocol family, socket type and socket behaviour for later
             */
            m_socket_creds.proto_family = get_os_proto_fam(ptype);
            m_socket_creds.socket_type  = get_os_socket_type(stype);
            m_socket_creds.socket_behaviour = sockb;
        }

        /*
         * dtor
         */
        virtual ~Socket() {
            if ( is_open() ) {
 #ifdef DEBUG
                std::cout << "Closing socket...\n";
#endif
                this->terminate();
            }
        }
        
        /**
         * @brief
         * Pure virtual method, must be reimplemented by lead classes,
         * in order to define the behaviour of the socket upon
         * opening it.
         */
        virtual void open() = 0;

        /**
         * @brief
         * Simple checker to acknowledge whether the
         * socket instance has an open connection.
         */
        bool is_open() {
            return m_socket_creds.is_open;
        }

        /**
         * @brief
         * Closes the socket. Does nothing, when socket is not open.
         */
        virtual void terminate() {

            if ( !is_open() ) {
                return;
            }

            auto result = close(m_socket_creds.socket_descriptor);

            if ( result == -1 ) {
                std::cerr << strerror(errno);
            }

            m_socket_creds.is_open = false;
        }

        protected:
       
        

        /*
         * Clears the message buffer and
         * fills it up with default value 0
         */
        void reset_message_buffer() {
            using namespace std;
            // attempt 1
            //m_socket_creds.buffer = std::vector<char>(m_socket_creds.buffer_size, 0);
            // attempt 2
            const int sz = m_socket_creds.buffer.size();
            m_socket_creds.buffer.clear();
            m_socket_creds.buffer.resize(sz);
            fill(begin(m_socket_creds.buffer), end(m_socket_creds.buffer), 0);
        }

        /*
         * Copies a string to the I/O socket buffer
         */
        auto string_to_buffer(const std::string &msg) -> void {
            
            //reset_message_buffer();

            std::copy(msg.begin(), msg.end(), m_socket_creds.buffer.begin());
        }

        /*
         * Copies the buffer content to a string instance and returns
         */
        auto buffer_to_string() -> std::string {

            std::string received;

            // convey buffer content to a string
            for ( auto &c: m_socket_creds.buffer ) {
                received += c;
            }
            
            // trim string
            trim(received);

            // Remove unused buffer content
            received.erase(std::find_if(received.begin(), received.end(), [](char &c){ return c == 0;}), received.end());

            //reset_message_buffer();

            return received;
        }

        /*
         * Prints content of buffer
         */
        auto print_buffer_content() {
 #ifdef DEBUG
            for ( auto &c: m_socket_creds.buffer ) {
                std::cout  << "'" << c << "' - ";
            }

            std::cout << std::endl;
 #endif
        }

        /**
         * Factory method, which creates a socket descriptor
         */
        [[nodiscard]] std::optional<int> 
        create_socket(const SockType &stype, const ProtoType &ptype, const SockBehaviour &sockb) {

           auto sock_opt = 0;

           if (sockb == SockBehaviour::NON_BLOCKING) {
               sock_opt |= SOCK_NONBLOCK;
               sock_opt |= SOCK_CLOEXEC;
           }

           auto socket_desc = socket(get_os_proto_fam(ptype),
                                     get_os_socket_type(stype) | sock_opt,
                                     0);
           
           if( socket_desc == -1 ) {
                return std::nullopt;
           }

           return std::optional<int>(socket_desc);
        }

        /*
         * Translates the SockType value to
         * a OS compatible socket type value.
         */
        int get_os_socket_type(const SockType &stype) {
            
            int socket_type{-1};

            switch(stype) {
                case SockType::TCP:
                    socket_type = SOCK_STREAM;
                break;
                case SockType::UDP:
                    socket_type = SOCK_DGRAM;
                break;

                // Add more here if required
            }

            return socket_type;
        }

        /*
         * Translates the ProtoType value to a
         * OS compatible protocol family value.
         */
        int get_os_proto_fam(const ProtoType &ptype) {
            
            int proto_type{-1};

            switch(ptype) {
                case ProtoType::NET:
                    proto_type = AF_INET;
                break;
                case ProtoType::LOCAL:
                    proto_type = AF_UNIX;
                break;

                // Add more here if required
            }

            return proto_type;
        }

        /*
         * Member
         */
        SocketCredentials m_socket_creds;
        ServerMessageProcessor m_smp;
        ClientMessageProcessor m_cmp;
    };


    /**
     * @brief The ClientSocket class
     * This class comprises all functionality
     * required for a client to connect to server.
     */
    class ClientSocket: public Socket {

        public:

        ClientSocket(const std::string_view ip_address, 
                     const int16_t &port, 
                     const SockType &stype, 
                     const ProtoType &ptype,
                     const int &buffer_sz,
                     const SockBehaviour &sockb,
                     std::shared_ptr<SharedClientSocketData> socket_data):
        Socket(ip_address, port, stype, ptype, buffer_sz, sockb),
        m_socket_data(socket_data) {

        }

        /**
         * @brief 
         * This method opens the connection to a
         * server based on the credentials, which were
         * passed at construction time.
         */
        virtual void open() override {
            
#ifdef DEBUG 
            std::cout << "Opening Client socket...\n";
#endif
            
            // Establish connection to server.
            if (m_socket_creds.socket_type == SOCK_STREAM) { // TCP
#ifdef DEBUG               
                std::cout << "Opening TCP socket...";
#endif
                // set protocol family
                m_socket_creds.saddrin.sin_family = m_socket_creds.proto_family;
                // set port
                m_socket_creds.saddrin.sin_port = htons(m_socket_creds.port);
                // set ip address
                auto ip_string = static_cast<std::string>(m_socket_creds.ip_address);
                inet_aton(ip_string.c_str(), &m_socket_creds.saddrin.sin_addr);


                auto state = connect(m_socket_creds.socket_descriptor, 
                                     (struct sockaddr*) &m_socket_creds.saddrin,
                                     sizeof(m_socket_creds.saddrin));
                if ( state < 0 ) {
                    std::cerr << "failed to open connection to server: " << strerror(errno) << "\n";
                    return;
                }
#ifdef DEBUG
                else {
                    std::cout << "connection to server established!\n";
                }
#endif

            } else if (m_socket_creds.socket_type == SOCK_DGRAM ) { // UDP
#ifdef DEBUG               
                std::cout << "Opening UDP socket...";
#endif          
                auto ip_string = static_cast<std::string>(m_socket_creds.ip_address);
                struct hostent *h = gethostbyname(ip_string.c_str());

                if ( !h ) {
                    std::cerr << "Unknown host: " << ip_string << "\n";
                    return;
                }

                /*
                 * set server socket
                 */
                m_socket_creds.saddrin.sin_family = h->h_addrtype;
                memcpy((char*) &m_socket_creds.saddrin.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
                m_socket_creds.saddrin.sin_port = htons(m_socket_creds.port);
                
                // set ip address
                //inet_aton(ip_string.c_str(), &m_socket_creds.saddrin.sin_addr);
                
                // prepare client socket
                m_socket_creds.udp_client_address.sin_family = m_socket_creds.proto_family;
                m_socket_creds.udp_client_address.sin_addr.s_addr = htonl(INADDR_ANY);
                m_socket_creds.udp_client_address.sin_port = htons(0);
                
                // bind client socket
                int state = bind(m_socket_creds.socket_descriptor,
                                 (struct sockaddr *) &m_socket_creds.udp_client_address,
                                 sizeof(m_socket_creds.udp_client_address));
                
                if ( state < 0 ) {
                    std::cerr << "Failed to bind client port: " << strerror(errno) << "\n";
                    return;
                }
#ifdef DEBUG
                else {
                    std::cout << "port successfully bound to application!\n";
                }
#endif
            }

            m_socket_creds.is_open = true;
        }

        /**
         * @brief
         * Public interface method, to trigger transmission of data.
         * The method is agnostic of the utilized connection protocol.
         * @param mtx - Reference to a std::mutex, shared with the calling thread context.
         */
        virtual void transmit(std::mutex &mtx) {
            
            if ( !this->is_open() ) {
                std::cerr << "Client socket is not open, call ClientSocket::open() first!\n";
                return;
            }

            if (m_socket_creds.socket_type == SOCK_STREAM) {
#ifdef DEBUG
                std::cout << "Transmitting via TCP connection...\n";
#endif
                transmit_tcp(mtx);
            } else if (m_socket_creds.socket_type == SOCK_DGRAM) {
#ifdef DEBUG
                std::cout << "Transmitting via UDP connection...\n";
#endif
                transmit_udp(mtx);
            } else {
                std::cerr << "Unimplemented socket type, cannot establish a client connection to server!\n";
            }
        }

        protected:
        /**
         * @brief
         * Parses the map state vector and returns
         * a vector with all available 1D positions
         * on the map.
         * @param map_state - const reference to a std::vector<int> which
         * denotes the state of each position on the game map.
         * @return std::optional<std::vector<int>> - A vector with all
         * free positions on the map as 1D positions. If the input vector is
         * empty, a std::nullopt value is returned.
         */
        [[nodiscard]]
        std::optional<std::vector<int>>
        free_positions_from_map_state(const std::vector<int> &map_state) {

            if (map_state.empty()) {
                assert(map_state.size() > 0);
                std::cerr << "map state vector must not be empty!\n";
                return std::nullopt;
            }
            
            // Count number of free slots in advance and create a vector
            // with matching size.
            auto sz = std::count(begin(map_state), end(map_state), 1);
            std::vector<int> free_food(sz, -1);
            auto out_it = begin(free_food);
            
            for (auto it = begin(map_state); it != end(map_state); ++it) { 
                if (*it == 1) {
                    *out_it = std::distance(begin(map_state), it);
                    ++out_it;
                }
            }
#ifdef DEBUG
            //debug print vector
            std::cout << "v: ";
            std::copy(free_food.begin(), free_food.end(), std::ostream_iterator<int>(std::cout, " "));
            std::cout << '\n';
#endif
            return std::optional<std::vector<int>>(free_food);
        }

        /**
         * @brief
         * Randomly selects a position in a std::vector<int>.
         * @param food_positions - rvalue std::vector<int>
         * @return std::optional<int> containing a randomly selected position.
         * Returns std::nullopt if the passed input vector is empty.
         */
        [[nodiscard]]
        std::optional<int>
        get_random_food_position(std::vector<int> &&food_positions) {
            
            if (food_positions.empty()) {
                assert(food_positions.size() > 0);
                std::cerr << "Food position vector must not be empty!\n";
                return std::nullopt;
            }
            auto upper = food_positions.size() - 1;
            return std::optional<int>(food_positions[ randn(0, upper).value_or(0) ]);
        }

        /**
         * @brief
         * Sends data which are in buffer to the 
         * specified server address. Buffered data is
         * deleted from buffer when this method is called,
         * regardless of whether the data transmission was succesfull.
         * @return true if sending of data succeeds, else false
         */
        bool send_buffered_data() {
            bool state = true;
            auto rc = sendto(m_socket_creds.socket_descriptor,
                             m_socket_creds.buffer.data(),
                             m_socket_creds.buffer.size(),
                             0,
                             (struct sockaddr *) &m_socket_creds.saddrin,
                             sizeof(m_socket_creds.saddrin));

            if ( rc < -1 ) {
                std::cerr << "failed to send data via UDP connection!\n";
                state = false;
            }
#ifdef DEBUG
            else {
                std::cout << "data sent!\n";
            }
#endif
            // Reset message buffer
            this->reset_message_buffer();
            return state;
        }

        /**
         * @brief
         * Receive data from a UDP server socket. This method can
         * run in blocking and non blocking mode.
         * @param poll_delay - Delay in milliseconds the method will
         * wait between to 2 calls to recvfrom, when in non-blocking mode.
         * Defaults to 50 milliseconds.
         * @return std::optional, either std::nullopt when socket is in blocking mode,
         * and no data was received or when in non-blocking mode and return
         * is not EAGAIN or EWOULDBLOCK. Otherwise optional contains the received
         * message string.
         */
        [[nodiscard]]
        std::optional<std::string> 
        receive_server_reply(const int &poll_delay = 50, const int &max_poll = -1) {
           
            if (poll_delay < 0) {
                std::cerr << "Invalid poll delay, cannot continue!\n";
                assert(poll_delay >= 0);
                return std::nullopt;
            }

            if (max_poll < -1) {
                std::cerr << "Max polls must be set to a positive integer or -1\n";
                assert(max_poll >= -1);
                return std::nullopt;
            }


            int polls_made = 0;
            bool not_received = true;
            auto recv_poll_delay = std::chrono::duration<int, std::milli>(poll_delay);
            socklen_t len = sizeof(m_socket_creds.saddrin);

            while (not_received) {
                auto sz = recvfrom(m_socket_creds.socket_descriptor,
                                   m_socket_creds.buffer.data(),
                                   m_socket_creds.buffer.size(),
                                   0,
                                   (struct sockaddr*) &m_socket_creds.saddrin,
                                   &len);

                if (sz < 0) { // non-blocking
                    if ( errno == EAGAIN || errno == EWOULDBLOCK ) {
#ifdef DEBUG
                        std::cout << "Waiting for server reply.\n";
#endif

                        if (max_poll > -1 && polls_made >= max_poll) {
#ifdef DEBUG
                            std::cout << "Waited max polls, stopping now.\n";
#endif
                            return std::nullopt;
                        } else {
                            ++polls_made;
                        }

                        // add poll delay here
                        std::this_thread::sleep_for(recv_poll_delay);
                        continue;
                    } else { // blocking
                        std::cerr << "no data received: " << strerror(errno) << "\n";
                        return std::nullopt;
                    }
                } else {
                    not_received = false;
                }
            }
           
            //print_buffer_content();
            auto srv_msg_string = this->buffer_to_string();
#ifdef DEBUG
            std::cout << "received: " << srv_msg_string << "\n";
#endif
            this->reset_message_buffer();
            return std::optional<std::string>(srv_msg_string);
        }

        /**
         * @brief
         * Call this method when client has not joined game server yet
         * and needs to get an ID to be registered to the server.
         * This method will send a REQ_JOIN message to the server
         * which either be replied with WELCOME, so that the client
         * can join or CON_CLOSE, in whoich case, the client is supposed
         * to terminate.
         * @return std::optional, which contains either an integer
         * with a positive ID value or a std::nullopt, when joining
         * did not succeed.
         */
        std::optional<int> join_game_server() {
#ifdef DEBUG           
            std::cout << "Trying to join the game server...\n";
#endif
            // holds assigned game id 
            int tmp_game_id{-1};

            // join request
            ClientMessage join_request({tmp_game_id, 
                                        ClientMessageType::REQ_JOIN, 
                                        false, 
                                        false,
                                        false,
                                        -1,
                                        SnakeDir::UP});

            // reset buffer
            this->reset_message_buffer();

            // stringify request
            auto join_request_msg = m_cmp.serialize(join_request);

            // copy messag to buffer
            this->string_to_buffer(join_request_msg);

            // send data in buffer
            auto success = send_buffered_data();

            if ( !success ) {
                std::cerr << "Failed to send REQ_JOIN request to server!\n";
                return std::nullopt;
            }

            auto reply_opt = receive_server_reply(50, 25);

            if ( !reply_opt ) {
                std::cerr << "no data received in time, terminating!\n";
                return std::nullopt;
            }

            auto srv_msg_string = reply_opt.value();
#ifdef DEBUG
            std::cout << "received: " << srv_msg_string << "\n";            
            std::cout << "Processing answer...";
#endif
            // Deserialize received message into ServerMessage object
            auto srv_msg = this->m_smp.deserialize( srv_msg_string );

            /*
             * Server's reply must either be WELCOME or CON_CLOSE,
             * anything else is an undefined state.
             */
            switch(srv_msg.mtype) {
                case ServerMessageType::WELCOME:
#ifdef DEBUG
                    std::cout << "server granted access!\n";
#endif
                    tmp_game_id = srv_msg.id;
                break;
                case ServerMessageType::CON_CLOSE:
                    std::cerr << "server denied to join, cannot continue!\n";
                    [[fallthrough]];
                default:
                    std::cerr << "unexpected message type!\n";
                    return std::nullopt;
            };
            
            return std::optional<int>(tmp_game_id);
       }

       /**
        * @brief
        * This will send a ClientMessageType::REQ_CLOSE to the
        * server, in order to let the server know, that the client wants to
        * disconnect prematurely.
        * @return boolean - true if server confirms in time else false
        */
       [[nodiscard]]
       bool send_user_exit_command(int game_id) {
            
           // Indicates success of method call
           bool state = true;

           do {
                
               if (game_id < 0) {
                    std::cerr << "invalid game id!\n";
                    assert(game_id >= 0);
                    state = false;
                    break;
                }

                // update request
                ClientMessage update({game_id, 
                                      ClientMessageType::REQ_CLOSE, 
                                      false, 
                                      false,
                                      false,
                                      -1,
                                      SnakeDir::UP});

                auto close_msg = m_cmp.serialize(update);
#ifdef DEBUG
                std::cout << "Created CLOSE message: " << close_msg << "\n";
#endif
                this->reset_message_buffer();
                
                // copy messag to buffer
                this->string_to_buffer(close_msg);

                // send data in buffer
                auto success = send_buffered_data();

                if ( !success ) {
                    std::cerr << "Failed to send REQ_CLOSE request to server!\n";
                    state = false;
                    break;
                }

                auto reply_opt = receive_server_reply(50, 25);

                if ( !reply_opt ) {
                    std::cerr << "no data received in time, terminating!\n";
                    state = false;
                    break;
                }

                auto srv_msg_string = reply_opt.value();
#ifdef DEBUG
                std::cout << "received: " << srv_msg_string << "\n";                
                std::cout << "Processing answer...";
#endif
                // Deserialize received message into ServerMessage object
                auto srv_msg = this->m_smp.deserialize( srv_msg_string );

                switch(srv_msg.mtype) {
                    case ServerMessageType::CON_CLOSE:
                    case ServerMessageType::CONFIRM:
#ifdef DEBUG
                        std::cout << "Server confirmed, client can leave.\n";
#endif
                        break;
                    default:
                        state = false;
                        std::cerr << "Server answered with unexpected type!\n";
                        break;
                }

            } while (1 == 0); 

           return state;
       }

       /**
        * @brief
        * This method sends a user action command to the server.
        * Only call this method, when the game id is known.
        * @param game_id - The ID the server assigned to the player client
        * @param has_new_dir - boolean, denotes if a new dir command is ready,
        * @param new_direction - SnakeDir enum value as new direction
        * @param plans_food_update - boolean, lets server know, that a food 
        *                            update is planned
        * @param has_food_update - boolean, lets server know that a new 
        *                          food position is contained in message
        * @param new_food_pos - signed integer, contains new food position
        * @return boolean, upon success returns true, else false
        */
       [[nodiscard]]
       bool send_user_command(const int &game_id,
                              bool has_new_dir,
                              SnakeDir new_direction, 
                              bool plans_food_update = false, 
                              bool has_food_update = false, 
                              const int &new_food_pos = -1) {

           // timing constants
           constexpr int poll_delay{50};
           constexpr int poll_attempts{25};
           // Indicates success of method call
           bool state = true;

           do {
                
               if (game_id < 0) {
                    std::cerr << "invalid game id!\n";
                    assert(game_id >= 0);
                    state = false;
                    break;
                }

                if (has_food_update && new_food_pos < 0) {
                    std::cerr << "Food position has an invalid value!";
                    assert(new_food_pos >= 0);
                    state = false;
                    break;
                }

                // update request
                ClientMessage update({game_id, 
                                      ClientMessageType::UPDATE, 
                                      has_new_dir, 
                                      plans_food_update,
                                      has_food_update,
                                      new_food_pos,
                                      new_direction});

                auto update_msg = m_cmp.serialize(update);
#ifdef DEBUG
                std::cout << "Created UPDATE message: " << update_msg << "\n";
#endif
                this->reset_message_buffer();
                
                // copy messag to buffer
                this->string_to_buffer(update_msg);

                // send data in buffer
                auto success = send_buffered_data();

                if ( !success ) {
                    std::cerr << "Failed to send UPDATE request to server!\n";
                    state = false;
                    break;
                }

                auto reply_opt = receive_server_reply(poll_delay, poll_attempts);

                if ( !reply_opt ) {
                    std::cerr << "no data received in time, terminating!\n";
                    state = false;
                    break;
                }

                auto srv_msg_string = reply_opt.value();
#ifdef DEBUG
                std::cout << "received: " << srv_msg_string << "\n";                
                std::cout << "Processing answer...";
#endif
                // Deserialize received message into ServerMessage object
                auto srv_msg = this->m_smp.deserialize( srv_msg_string );

                /*
                 * Server's reply must either be CONFIRM or CON_CLOSE,
                 * anything else is an undefined state.
                 */
                if (srv_msg.mtype == ServerMessageType::CONFIRM) {
                    assert(game_id == srv_msg.id);
#ifdef DEBUG
                    std::cout << "Server confirmed message!\n";
#endif
                    if (srv_msg.has_map_update) {
                        auto ffp = free_positions_from_map_state( srv_msg.map_state );
                        
                        if (!ffp) {
                            break;
                        }

                        auto new_food_pos_opt = get_random_food_position( std::move(ffp.value()) );

                        if ( !new_food_pos_opt ) {
                            std::cerr << "Invalid food position!\n";
                            break;
                        }
#ifdef DEBUG
                        std::cout << "Randomly picked food position is: " << new_food_pos_opt.value() << "\n";
#endif
                        // issue another user command, to notify server about 
                        // new food position
                        if ( !send_user_command(game_id,
                                                false,
                                                SnakeDir::UP,
                                                false,
                                                true,
                                                new_food_pos_opt.value()) ) {
                            std::cerr << "Failed to pass new food position to server!\n";
                            break;
                        }
                    }

                } else if (srv_msg.mtype == ServerMessageType::CON_CLOSE) {
                    assert(game_id == srv_msg.id);
                    if (srv_msg.player_dead) {
                        std::cerr << "Snake died, terminating!\n";
                    } else {
                        std::cerr << "Server terminated session!\n";
                    }
                    state = false;
                } else {
                    std::cerr << "unexpected message type, cannot continue!\n";
                    state = false;
                }
           } while(1 == 0);

           return state;
       }
       
        /**
         * @brief
         * Main communication method of client
         * for UDP communication. This method is supposed to run
         * in an own thread context.
         * @param mtx - std::mutex, shared with calling context,
         *              to synchronize data exchange.
         */
        void transmit_udp(std::mutex &mtx) {

            using namespace std;

            // denotes if connection shall be closed
            bool close_connection = false;
            // denotes if server game joined
            bool joined_server    = false;
            // denotes if client plans food update
            std::atomic_bool plans_food_update = false;
            // holds the game ID, the server assigned to client
            int game_id           = -1;
            // poll delay between 2 user input calls
            auto user_input_delay = std::chrono::duration<int, std::milli>(5);

            /*
             * helper, to check if terminations was
             * requested in main thread.
             */
            auto check_termination_scheduled = 
                [this]() -> bool {
                    // No lock required here, as bools are atomic
                    return m_socket_data->terminate;
            };

            /*
             * Helper to notifiy main thread that it shall
             * terminate as soon as possible.
             */
            auto schedule_client_termination = [this, &mtx]() {
#ifdef DEBUG
                std::cout << "Scheduling client termination...\n";
#endif
                std::lock_guard<std::mutex> lock(mtx);
                m_socket_data->terminate = true;
            };

            /*
             * Helper checks for user input updates.
             * Since dir_update is an atomic_bool, we can
             * avoid locking when there is no new dir, which
             * will be most of the time the case.
             */
            auto check_new_user_input = 
                [this, &mtx] () -> std::optional<SnakeDir> {
                
                if (m_socket_data->dir_update) {
                    std::lock_guard<std::mutex> lock(mtx);
                    m_socket_data->dir_update = false;
                    return std::optional<SnakeDir>(m_socket_data->next_snake_dir);
                }

                return std::nullopt;
            };

            /*
             * Food update timer
             */
            Timer food_timer;
            
            // this lambda executes every 5 seconds
            auto indicate_food_update = [&plans_food_update](){
#ifdef DEBUG
                std::cout << "Food update triggered!\n";
#endif
                plans_food_update = true;
            };

            //start timer
            food_timer.set_interval(indicate_food_update, 5000);

            /*
             * Socket main loop
             */
            while (!close_connection) {

                close_connection = check_termination_scheduled(); 

                /*
                 * Case happens when main thread wants to quit while
                 * running a game. Here message is send to the server
                 * first to tell him that the client is about to
                 * exit. The server may delete the players snake and
                 * confirm the client exit.
                 */
                if (close_connection && joined_server) {
                    
                    auto srv_reply = send_user_exit_command(game_id);

                    if (!srv_reply) {
                        std::cerr << "Server did non confirm premature client exit. Terminating anyway.\n";
                    } 
                    
                    joined_server = false;
                    game_id = -1;
                    continue;
                }

                /*
                 * Initially try to join server by sending a REQ_JOIN message.
                 * If the server sends a WELCOME message back, the player may play
                 * otherwise the client will terminate.
                 */
                if ( !joined_server ) {
                    auto id_opt = join_game_server();

                    if (!id_opt) {
                        std::cerr << "Failed to join the game server, terminating.\n";
                        close_connection = true;
                        continue;
                    }

                    // Success! Store assigned game ID
                    game_id = id_opt.value();
                    joined_server = true;
                }

                /*
                 * This point is only reached, when the server
                 * assigned a game ID to the client.
                 */

                // Check for necessary food update first
                if (!close_connection && plans_food_update) {
                    
                    if ( !send_user_command(game_id, false, SnakeDir::UP, plans_food_update) ) {
                        std::cerr << "Failed to request food update!\n";
                        close_connection = true;
                    }

                    plans_food_update = false;
                }

                // now check for user input in main thread
                auto user_input_opt = check_new_user_input(); 

                // bail out early if no user input present
                if (!user_input_opt) {
                    std::this_thread::sleep_for(user_input_delay);
                    continue;
                }

                // new user input, send to server
                if ( !send_user_command(game_id, true, user_input_opt.value()) ) {
                    std::cerr << "Failed or server closed session!\n";
                    close_connection = true;
                }
            }

            std::cout << "Socket closed. Please confirm by pressing any key.\n";
            schedule_client_termination();
        }

        /*
         *
         */
        void transmit_tcp(std::mutex& mtx) {
           
            using namespace std;
/*
             * Helper checks for user input updates.
             * Since dir_update is an atomic_bool, we can
             * avoid locking when there is no new dir, which
             * will be most of the time the case.
             */
            auto check_new_user_input = 
                [this, &mtx] () -> std::optional<SnakeDir> {
                
                if (m_socket_data->dir_update) {
                    std::lock_guard<std::mutex> lock(mtx);
                    m_socket_data->dir_update = false;
                    return std::optional<SnakeDir>(m_socket_data->next_snake_dir);
                }

                return std::nullopt;
            };

            bool close_connection = false;
            const vector<string> allowed_inputs({"w", "a", "s", "d", "q", "c"});
            
            do { 

                // get user input
                //auto user_input_opt = read_user_input(allowed_inputs);

                auto user_input_opt = check_new_user_input();
                if ( !user_input_opt ) {
                    std::cerr << "Invalid user input!\n";
                    continue;
                }

                auto user_input = user_input_opt.value();

                /*
                if (user_input == "q" || user_input == "c") {
                    close_connection = true;
                }
                */

                // reset buffer
                this->reset_message_buffer();

                // copy message to buffer
                //this->string_to_buffer(user_input);
                this->string_to_buffer("CHANGE THIS");

                auto check_sz = send(m_socket_creds.socket_descriptor, 
                                  m_socket_creds.buffer.data(), 
                                  m_socket_creds.buffer.size(),
                                  0);

                if ( check_sz != m_socket_creds.buffer.size() ) {
                    cerr << "Client: Failed to send data of size " << m_socket_creds.buffer.size() << "\n";
                    cerr << "Client: Errorcode (" << errno << ") > " << strerror(errno);
                    assert(check_sz != m_socket_creds.buffer.size());
                }

                // reset message buffer
                this->reset_message_buffer();

                
                // wait for server reply
                recv(m_socket_creds.socket_descriptor,
                     m_socket_creds.buffer.data(),
                     m_socket_creds.buffer.size(),
                     0);
                
                //this->print_buffer_content();

                auto server_reply = this->buffer_to_string();
#ifdef DEBUG
                cout << "Client: server replied with " << quoted(server_reply) << "\n";
#endif
            } while(!close_connection);
        }

        /*
         * Member
         */
        std::shared_ptr<SharedClientSocketData> m_socket_data;

    };

    /**
     * @brief The ServerSocket class
     * This class comprises all functionality
     * required by a server to accept incoming client
     * connection attempts.
     */
    class ServerSocket: public Socket {

        public:

        ServerSocket(const std::string_view ip_address, 
                     const int16_t &port, 
                     const SockType &stype, 
                     const ProtoType &ptype, 
                     const int &buffer_sz,
                     const SockBehaviour &sockb,
                     std::shared_ptr<SharedSocketData> socket_data):
        Socket(ip_address, port, stype, ptype, buffer_sz, sockb),
        m_socket_data(socket_data) {

        }

        /**
         * @brief Open the server socket
         * Sets up the server socket connection. 
         * After a call to this method, the server is ready
         * to listen for incoming client request. However
         * the server socket is not put into listen mode yet.
         */
        virtual void open() override {
#ifdef DEBUG
            std::cout << "Opening Server socket...\n";
#endif 
            const int y = 1;

            setsockopt(m_socket_creds.socket_descriptor,
                       SOL_SOCKET,
                       SO_REUSEADDR,
                       &y,
                       sizeof(int));
            
            m_socket_creds.saddrin.sin_family = m_socket_creds.proto_family;

            if (m_socket_creds.socket_type == SOCK_STREAM) {
#ifdef DEBUG
                std::cout << "Socket type set to TCP socket.\n";
#endif
                m_socket_creds.saddrin.sin_addr.s_addr = INADDR_ANY;
            } else if (m_socket_creds.socket_type == SOCK_DGRAM) {
#ifdef DEBUG
                std::cout << "Socket type set to UDP socket\n";
#endif
                m_socket_creds.saddrin.sin_addr.s_addr = htonl(INADDR_ANY);
            }

            m_socket_creds.saddrin.sin_port = htons(m_socket_creds.port);

            auto state = bind(m_socket_creds.socket_descriptor, 
                              (struct sockaddr*) &m_socket_creds.saddrin, 
                              sizeof(m_socket_creds.saddrin));
            if (state < 0) {
                std::cerr << "The port " << m_socket_creds.port << " can not be bound: " << strerror(errno) << "\n";
                return;
            }
#ifdef DEBUG
            else {
                std::cout << "Server port " << m_socket_creds.port << " bound to application.\n";
            }
#endif

            m_socket_creds.is_open = true;
        }

        /**
         * @brief I/O routine of server
         * A call to this method will cause the server to start listening for incoming
         * client connections. A successful call to open() must precede calls to this method.
         */
        virtual void await_connections(std::mutex &mtx) {

            if ( !this->is_open() ) {
                std::cerr << "Server socket is not open, call ServerSocket::open() first!\n";
                return;
            }

            if (m_socket_creds.socket_type == SOCK_STREAM) {
#ifdef DEBUG
                std::cout << "Listening for TCP connections...\n";
#endif
                await_tcp(mtx);
            } else if (m_socket_creds.socket_type == SOCK_DGRAM) {
#ifdef DEBUG
                std::cout << "Listening for UDP connections...\n";
#endif
                await_udp(mtx);
            } else {
                std::cerr << "Unimplemented socket type, cannot start a listening connection on server!\n";
            }
        }

        protected:

        /**
         * @brief Server I/O method for UDP connections
         * @param mtx mutex shared with parent context
         */
        virtual void await_udp(std::mutex &mtx) {

            bool stop_server = false;
            const auto poll_delay = std::chrono::duration<int, std::milli>(50);

            /*
             * Checks if socket termination was request from main thread
             */
            auto check_terminate = [this, &mtx, &stop_server]() {
                std::lock_guard<std::mutex> lock(mtx);
                if ( m_socket_data->terminate ) {
#ifdef DEBUG
                    std::cout << "Main thread asked to terminate!\n";
#endif
                    stop_server = true;
                }
            };

            /*
             * Writes an incoming client message to shared data structure
             */
            auto write_client_message_to_shared = [this, &mtx] (const std::string &client_message) {
                std::lock_guard<std::mutex> lock(mtx);
                m_socket_data->client_messages.push_back( m_cmp.deserialize(client_message) );
                m_socket_data->client_update = true;
            };
            
            /*
             * Polls main thread for a game update.
             * Use max poll parameter, to limit polling
             * to some user defined limit.
             */
            auto check_for_game_update = [this, &poll_delay, &mtx](const int &max_poll = -1) -> std::optional<std::vector<ServerMessage>> {
               

                if (max_poll < -1) {
                    std::cerr << "maximum of polls must not be a negative number other than -1.\n";
                    assert(max_poll >= -1);
                    return std::nullopt;
                }

                int polls_made = 0;

                while (!m_socket_data->server_update) {

                    if (max_poll != -1 && polls_made >= max_poll) {

#ifdef DEBUG
                        std::cout << "No server update in time, stop polling.\n";
#endif

                        return std::nullopt;
                        
                    } else {
                        ++polls_made;
                    }

                    std::this_thread::sleep_for(poll_delay);
#ifdef DEBUG
                    std::cout << "Waiting for game thread...\n";
#endif
                }

                std::lock_guard<std::mutex> lock(mtx);
                
                auto ret = m_socket_data->server_messages;
                
                m_socket_data->server_messages.clear();
                m_socket_data->server_update = false;
                return std::optional<std::vector<ServerMessage>>(ret);
            };
#ifdef DEBUG      
            std::cout << "Starting UDP Server...\n";
#endif
            while (!stop_server) {

                socklen_t len = sizeof(m_socket_creds.udp_client_address);
                auto sz = recvfrom(m_socket_creds.socket_descriptor,
                                   m_socket_creds.buffer.data(),
                                   m_socket_creds.buffer.size(),
                                   0,
                                   (struct sockaddr *) &m_socket_creds.udp_client_address,
                                   &len);
                
                if (sz < 0) {
                    if (errno == EWOULDBLOCK || errno == EAGAIN) {
                        std::this_thread::sleep_for(poll_delay);
                        check_terminate();
                        continue;
                    } else  {
                        std::cerr << "Failed to receive data from client!\n";
                        stop_server = true;
                        continue;
                    }
                } 

                auto client_msg_str = buffer_to_string();
#ifdef DEBUG
                std::cout << "Client message received: " << std::quoted(client_msg_str) << "\n";
#endif
                // pass all client messages to main thread
                write_client_message_to_shared(client_msg_str);
              
                // clean buffer before writing back
                reset_message_buffer();

                // poll for main thread response, max poll 12 == 600 ms,
                // more than one game cycle
                auto server_messages_opt = check_for_game_update(12);

                if ( !server_messages_opt ) {
                    std::cout << "No game update received.\n";
                    continue;
                }

                // process all server messages 
                for (auto &sm: server_messages_opt.value()) {
#ifdef DEBUG
                    std::cout << "Sending message...\n";
#endif
                    // serialize message
                    auto msg = m_smp.serialize(sm);
#ifdef DEBUG
                    std::cout << "Created new server message: " << std::quoted(msg) << "\n";
#endif
                    // write request to buffer
                    this->string_to_buffer(msg);
#ifdef DEBUG                   
                    std::cout << "Sending server msg...";
#endif
                    // send request
                    auto rc = sendto(m_socket_creds.socket_descriptor,
                                     m_socket_creds.buffer.data(),
                                     m_socket_creds.buffer.size(),
                                     0,
                                     (struct sockaddr *) &m_socket_creds.udp_client_address,
                                     sizeof(m_socket_creds.udp_client_address));

                    if ( rc < -1 ) {
                        std::cerr << "failed to send data via UDP connection!\n";
                    } 
#ifdef DEBUG
                    else {
                        std::cout << "data sent!\n";
                    }
#endif
                    // Reset message buffer
                    this->reset_message_buffer();
                }

                // Check if main thread aske to terminate
                check_terminate();
            }
        }

        /**
         * @brief 
         * Server I/O method for TCP connections.
         * @param mtx a mutex shared with parent context.
         */
        virtual void await_tcp(std::mutex &mtx) {
            
            // default server answer
            const std::string answer("ok");
            //determines whether to stop the whole socket thread
            bool stop_server(false);
            // determines whether to close an open connection
            bool close_con(false);
            // socket loop delay in ms
            auto d = std::chrono::duration<int, std::milli>(250);
            auto poll_delay = std::chrono::duration<int, std::milli>(50);

            /*
             * Checks if socket termination was request from main thread
             */
            auto check_terminate = [this, &mtx, &stop_server, &close_con]() {
                std::lock_guard<std::mutex> lock(mtx);
                if ( m_socket_data->terminate ) {
#ifdef DEBUG
                    std::cout << "Main thread asked to terminate!\n";
#endif
                    close_con = stop_server = true;
                }
            };

            /*
             * Writes an incoming client message to shared data structure
             */
            auto write_client_message_to_shared = [this, &mtx] (const std::string &client_message) {
                std::lock_guard<std::mutex> lock(mtx);
                //m_socket_data->client_messages.clear();
                m_socket_data->client_messages.push_back( m_cmp.deserialize(client_message) );
                m_socket_data->client_update = true;
            };

            /*
             * Polls main thread for a game update
             */
            auto check_for_server_update = [this, &poll_delay, &mtx]() -> std::vector<ServerMessage> {
                do {
                    std::this_thread::sleep_for(poll_delay);
#ifdef DEBUG
                    std::cout << "Waiting for game thread...\n";
#endif
                } while (!m_socket_data->server_update);

                std::lock_guard<std::mutex> lock(mtx);
                auto ret = m_socket_data->server_messages;
                m_socket_data->server_messages.clear();
                m_socket_data->server_update = false;
                return ret;
            };

            // put socket in listening mode
            listen(m_socket_creds.socket_descriptor, 5);
            // structure size for later use
            socklen_t address_sz = sizeof(struct sockaddr_in);


            /// @todo change this to proper flag value later
            while(!stop_server) {
            
                int new_socket = -1;

                // Accept incoming connection request from client
                switch (m_socket_creds.socket_behaviour) {
                    case SockBehaviour::BLOCKING:
                        new_socket = accept(m_socket_creds.socket_descriptor,
                                            (struct sockaddr *) &m_socket_creds.saddrin,
                                            &address_sz);
                    break;
                    case SockBehaviour::NON_BLOCKING:
                        
                        new_socket = accept4(m_socket_creds.socket_descriptor,
                                            (struct sockaddr *) &m_socket_creds.saddrin,
                                            &address_sz, 
                                            SOCK_NONBLOCK);

                        if (new_socket == -1) {
                            
                            if (errno == EWOULDBLOCK || errno == EAGAIN) {
                                std::this_thread::sleep_for(d);
                                check_terminate();
                                continue;
                            }
                        }
                    break;
                    default:
                        std::cerr << "Other methods apart from blocking and non blocking are not implemented right now.\n";
                        return;
                    break;
                };

                std::string client(inet_ntoa(m_socket_creds.saddrin.sin_addr));
#ifdef DEBUG
                if (new_socket > 0) {
                    std::cout << "Server: client " << client << " connected!\n";
                }
#endif
                /*
                 * @todo
                 * This is just for testing, the socket.
                 * It reads the message from the client and returns it.
                 * It shall later be replaced with logic to write any message required.
                 */
                while(!close_con) {

                    /*
                     * Read client message
                     */
                    this->reset_message_buffer();

                    ssize_t sz = recv(new_socket, 
                                      m_socket_creds.buffer.data(),
                                      m_socket_creds.buffer.size(),
                                      0);
                    
                    if (m_socket_creds.socket_behaviour == SockBehaviour::NON_BLOCKING && sz == -1) {
                        if (errno == EWOULDBLOCK || errno == EAGAIN) {
                            std::this_thread::sleep_for(d);
                            check_terminate();
                            continue;
                        }
                    }

                    // sz 0 means client disconnected
                    if ( sz == 0 ) {
#ifdef DEBUG
                        std::cout << "Server: Client disconnected!" << std::endl;
#endif
                        close_con = true;
                        continue;
                    }

                    // Convey buffer content to string
                    std::string received = this->buffer_to_string();
                    write_client_message_to_shared( buffer_to_string() );
                    //output received message
                    std::cout << "Server: received message from client " << client << ": " << received << "(" << received.size() << ")" << std::endl;


                    /*
                     * Prepare answer to client
                     */
                    this->reset_message_buffer();

                    /*
                     * Poll for answer
                     */
                    check_for_server_update();
                    
                    // copy answer to buffer
                    //this->string_to_buffer(answer);

                    // Send answer to client
                    send(new_socket, m_socket_creds.buffer.data(), m_socket_creds.buffer.size(), 0);
                    
                    /*
                     * Handle server operation
                     */
                    if (received == "c") {
#ifdef DEBUG
                        std::cout << "Server: received close command, expecting client to terminate connection now.\n";
#endif
                        close_con = true;
                    }

                    if (received == "q") {
#ifdef DEBUG
                        std::cout << "Server: received quit command, terminating server...!" << std::endl;
#endif
                        close_con = true;
                        stop_server = true;
                    }

                    /*
                     * Critical: Mutex locked region
                     */
                    check_terminate();
                };

                close_con = false;
#ifdef DEBUG
                if ( !stop_server ) {
                    std::cout << "Server: closed connection...waiting for new...\n";
                }
#endif
            }

            if (stop_server) {
                std::lock_guard<std::mutex> lock(mtx);
                m_socket_data->terminate = stop_server; 
            }
        }

        /*
         * Member
         */
        std::shared_ptr<SharedSocketData> m_socket_data;
    };
}

#endif //SOCKET_HPP
