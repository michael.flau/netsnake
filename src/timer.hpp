/**
 * @author Michael Flau
 * @date december 2022
 * @file timer.hpp
 * Contains all logic for a simple timer class.
 * Requires pthreads to be enabled to work.
 *
 * Basic idea taken from: https://www.fluentcpp.com/2018/12/28/timer-cpp/
 */

#ifndef TIMER_HPP
#define TIMER_HPP

#include "common.hpp"
#include <thread>
#include <atomic>

namespace netsnake {

class Timer final {

    public:

        ~Timer() {
#ifdef DEBUG
            std::cout << "Stopping timer, and destroying\n";
#endif
            this->stop();
        }

        template <typename func>
        void set_interval(func function, int interval) {
            this-> mb_destroy = false;

            std::thread timer_thread ( [=](){
                
                    while (true) {

                        if (this->mb_destroy) {
                            return;
                        }

                        std::this_thread::sleep_for(std::chrono::milliseconds(interval));

                        if (this->mb_destroy) {
                            return;
                        }

                        function();
                    }
            });

            timer_thread.detach();
        }

        void stop() {
            mb_destroy = true;
        }

    private:
        std::atomic_bool mb_destroy = false;
};
}
#endif //TIMER_HPP
